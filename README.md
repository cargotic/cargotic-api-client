# Cargotic API Client

> :warning: This client is under heavy development and some of its parts may not be
> 100% functional!

Isomorphic client code for interacting with our cloud services

## Installation

To install the package, you'll first need to add our public npm repository to
your npm configuration. You can do so by typing the following.

```bash
npm config set @cargotic-oss:registry https://npm.cargotic.dev
```

After that, installing the package is as simple as:

```
npm i @cargotic-oss/api-client
```

## Authentication

_This section is currently being worked on and will be available soon. Stay
tuned._

## Usage

Here's a sample code for fetching your car expenses.

```typescript
import { createClient } from "@cargotic-oss/api-client";

const main = async () => {
  const client = createClient();
  const [january] = await client.vehicle.getYearlyVehicleExpenses({
    vehicleId: 23,
    year: 2020
  });

  if (!january) {
    console.log("You have not logged any expenses for January!");
  } else {
    const { total, currency } = january;

    console.log(
      `You have paid total of ${total} ${currency} for the vehicle in January.`
    );
  }
}

main();
```

# Publish to NPM registry

This repository has full automation through Gitlab CI/CD.

Steps to publish new version of package to NPM registry

1. Increase package version in `package.json`
2. Update version in `package-lock.json` via `npm install`
3. Merge to `master` branch
4. Tag commit on `master` branch
