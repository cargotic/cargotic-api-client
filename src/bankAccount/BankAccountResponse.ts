import { Id } from "@cargotic-oss/model";
import { Currency } from "@cargotic-oss/common";

interface BankAccountResponse {
  bankAccountId: Id;
  bban: string;
  iban: string;
  companyId: number;
  creatorId: number;
  currency: Currency;
  isDeleted: boolean;
}

export default BankAccountResponse;
