import { AxiosInstance } from "axios";

import { Id, BankAccountForm } from "@cargotic-oss/model";

import getBankAccounts from "./getBankAccounts";
import postBankAccount from "./postBankAccount";
import deleteBankAccount from "./deleteBankAccount";
import putBankAccount from "./putBankAccount";

const createBankAccountClient = (axios: AxiosInstance) => ({
  getBankAccounts: () => getBankAccounts({ axios }),
  deleteBankAccount: ({ id }: { id: Id }) => deleteBankAccount({ axios, id }),
  postBankAccount: ({
    bankAccount
  }: {
    bankAccount: BankAccountForm
  }) => postBankAccount({
    axios,
    bankAccount
  }),
  putBankAccount: ({
    id,
    bankAccount
  }: {
    id: Id,
    bankAccount: BankAccountForm
  }) => putBankAccount({
    axios,
    id,
    bankAccount
  })
});

export default createBankAccountClient;
