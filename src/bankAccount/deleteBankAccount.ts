import { AxiosInstance } from "axios";

import { Id } from "@cargotic-oss/model";

import { httpDelete } from "../http";

const deleteBankAccount = async ({
  id,
  axios
}: {
  id: Id,
  axios: AxiosInstance,
}) => {
  await httpDelete({
    axios,
    url: `company-profile/banking/${id}`
  });
};

export default deleteBankAccount;
