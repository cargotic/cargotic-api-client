import { BankAccount } from "@cargotic-oss/model";
import BankAccountResponse from "./BankAccountResponse";

const deserializeBankAccount = (
  {
    bankAccountId,
    bban,
    iban,
    companyId,
    creatorId,
    currency,
    isDeleted
  }: BankAccountResponse
) => ({
  id: bankAccountId,
  bban,
  iban,
  companyId,
  creatorId,
  currency,
  isDeleted
}) as BankAccount;

export default deserializeBankAccount;
