import { AxiosInstance } from "axios";
import { httpGet } from "../http";
import deserializeBankAccount from "./deserializeBankAccount";
import BankAccountResponse from "./BankAccountResponse";

const getBankAccounts = async ({
  axios
}: {
  axios: AxiosInstance,
}) => {
  const { data } = await httpGet<BankAccountResponse[]>({
    axios,
    url: "company-profile/banking"
  });

  return typeof data === "undefined"
    ? undefined
    : data.map(deserializeBankAccount);
};

export default getBankAccounts;
