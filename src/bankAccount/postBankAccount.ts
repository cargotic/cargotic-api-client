import { AxiosInstance } from "axios";
import { BankAccountForm } from "@cargotic-oss/model";

import { httpPost } from "../http";

import deserializeBankAccount from "./deserializeBankAccount";
import BankAccountResponse from "./BankAccountResponse";

const postBankAccount = async ({
  bankAccount,
  axios
}: {
  bankAccount: BankAccountForm,
  axios: AxiosInstance,
}) => {
  const { data } = await httpPost<BankAccountResponse>({
    axios,
    data: bankAccount,
    url: "company-profile/banking"
  });

  return typeof data === "undefined" ? undefined : deserializeBankAccount(data);
};

export default postBankAccount;
