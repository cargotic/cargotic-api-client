import { AxiosInstance } from "axios";

import { Id, BankAccountForm } from "@cargotic-oss/model";

import { httpPut } from "../http";

import deserializeBankAccount from "./deserializeBankAccount";
import BankAccountResponse from "./BankAccountResponse";

const postBankAccount = async ({
  id,
  bankAccount,
  axios
}: {
    id: Id,
    bankAccount: BankAccountForm,
    axios: AxiosInstance,
}) => {
  const { data } = await httpPut<BankAccountResponse>({
    axios,
    data: bankAccount,
    url: `company-profile/banking/${id}`
  });

  return typeof data === "undefined" ? undefined : deserializeBankAccount(data);
};

export default postBankAccount;
