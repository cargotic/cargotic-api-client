import {
  ContactCommentaryActivity,
  ContactCreateActivity,
  ContactEmployeeCreateActivity,
  ContactEmployeeDeleteActivity,
  ContactEmployeeUpdateActivity,
  ContactUpdateActivity
} from "@cargotic-oss/model";

import { CommentaryData } from "../social";

interface ContactCommentaryActivityData extends Omit<
  ContactCommentaryActivity,
  "commentary"
> {
  commentary: CommentaryData;
}

interface ContactCreateActivityData extends Omit<
  ContactCreateActivity,
  "createdAt"
> {
  createdAt: string;
}

interface ContactEmployeeCreateActivityData extends Omit<
  ContactEmployeeCreateActivity,
  "createdAt"
> {
  createdAt: string;
}

interface ContactEmployeeDeleteActivityData extends Omit<
  ContactEmployeeDeleteActivity,
  "createdAt"
> {
  createdAt: string;
}

interface ContactEmployeeUpdateActivityData extends Omit<
  ContactEmployeeUpdateActivity,
  "createdAt"
> {
  createdAt: string;
}

interface ContactUpdateActivityData extends Omit<
  ContactUpdateActivity,
  "createdAt"
> {
  createdAt: string;
}

type ContactActivityData =
  ContactCommentaryActivityData
  | ContactCreateActivityData
  | ContactEmployeeCreateActivityData
  | ContactEmployeeDeleteActivityData
  | ContactEmployeeUpdateActivityData
  | ContactUpdateActivityData;

export default ContactActivityData;
