import { Contact } from "@cargotic-oss/model";

interface ContactData extends Omit<
  Contact, "createdAt" | "insuranceExpiresAt"
> {
  insuranceExpiresAt?: string;
  createdAt: string;
}

export default ContactData;
