import { ContactMatchQueryResult } from "@cargotic-oss/model";

import ContactData from "./ContactData";

interface ContactMatchQueryResultData
  extends Omit<ContactMatchQueryResult, "matches"> {

  matches: ContactData[];
}

export default ContactMatchQueryResultData;
