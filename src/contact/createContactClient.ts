import { Currency } from "@cargotic-oss/common";
import {
  ContactForm,
  ContactEmployeeForm,
  ContactEmployeeMatchQuery,
  ContactEmployeeSuggestQuery,
  ContactMatchQuery,
  ContactSharesForm,
  ContactSuggestQuery,
  CommentaryForm,
  TimePeriod,
  DateRange
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import deleteContact from "./deleteContact";
import deleteContactCommentary from "./deleteContactCommentary";
import deleteContactEmployee from "./deleteContactEmployee";
import getContactActivity from "./getContactActivity";
import getContactDuplicate from "./getContactDuplicate";
import getContactOrderBalanceHighlight from "./getContactOrderBalanceHighlight";
import getContactShipmentCommissionHighlight
  from "./getContactShipmentCommissionHighlight";
import getContactPrefill from "./getContactPrefill";
import getContact from "./getContact";
import getContactShares from "./getContactShares";
import getContactShipmentBalanceHighlight
  from "./getContactShipmentBalanceHighlight";

import getContactShipmentsHighlight from "./getContactShipmentsHighlight";
import getContactTags from "./getContactTags";
import postContact from "./postContact";
import postContactAvatar from "./postContactAvatar";
import postContactCommentary from "./postContactCommentary";
import postContactEmployee from "./postContactEmployee";
import postContactEmployeeMatchQuery from "./postContactEmployeeMatchQuery";
import postContactEmployeeSuggestQuery from "./postContactEmployeeSuggestQuery";
import postContactMatchQuery from "./postContactMatchQuery";
import postContactShares from "./postContactShares";
import postContactSuggestQuery from "./postContactSuggestQuery";
import putContact from "./putContact";
import putContactCommentary from "./putContactCommentary";
import putContactEmployee from "./putContactEmployee";

const createContactClient = (axios: AxiosInstance) => ({
  deleteContact: ({
    contactId
  }: {
    contactId: number
  }) => deleteContact({ axios, contactId }),

  deleteContactCommentary: ({
    contactId,
    commentaryId
  }: {
    contactId: number,
    commentaryId: number
  }) => deleteContactCommentary({ axios, contactId, commentaryId }),

  deleteContactEmployee: ({
    contactId,
    employeeId
  }: {
    contactId: number,
    employeeId: number
  }) => deleteContactEmployee({ axios, contactId, employeeId }),

  getContact: ({
    contactId
  }: {
    contactId: number
  }) => getContact({ axios, contactId }),

  getContactActivity: ({
    contactId
  }: {
    contactId: number
  }) => getContactActivity({ axios, contactId }),

  getContactDuplicate: ({
    ico,
    dic
  }: {
    ico?: string,
    dic?: string
  }) => getContactDuplicate({ axios, ico, dic }),

  getContactOrderBalanceHighlight: ({
    contactId,
    currency,
    period,
    timeZone,
    range
  }: {
    contactId: number,
    currency: Currency,
    period: TimePeriod,
    timeZone?: string,
    range?: DateRange
  }) => getContactOrderBalanceHighlight({
    axios,
    contactId,
    currency,
    period,
    timeZone,
    range
  }),

  getContactShipmentCommissionHighlight: ({
    contactId,
    currency,
    period,
    timeZone,
    range
  }: {
    contactId: number,
    currency: Currency,
    period: TimePeriod,
    timeZone?: string,
    range?: DateRange
  }) => getContactShipmentCommissionHighlight({
    axios,
    contactId,
    currency,
    period,
    timeZone,
    range
  }),

  getContactPrefill: ({
    ico,
    dic
  }: {
    ico?: string,
    dic?: string
  }) => getContactPrefill({ axios, ico, dic }),

  getContactShares: ({
    contactId
  }: {
    contactId: number
  }) => getContactShares({ axios, contactId }),

  getContactShipmentBalanceHighlight: ({
    contactId,
    currency,
    period,
    timeZone,
    range
  }: {
    contactId: number,
    currency: Currency,
    period: TimePeriod,
    timeZone?: string,
    range?: DateRange
  }) => getContactShipmentBalanceHighlight({
    axios,
    contactId,
    currency,
    period,
    timeZone,
    range
  }),

  getContactShipmentsHighlight: ({
    contactId,
    period,
    timeZone,
    range
  }: {
    contactId: number,
    period: TimePeriod,
    timeZone?: string,
    range?: DateRange
  }) => getContactShipmentsHighlight({
    axios,
    contactId,
    period,
    timeZone,
    range
  }),

  getContactTags: () => getContactTags({ axios }),

  postContact: ({
    contact
  }: {
    contact: ContactForm
  }) => postContact({ axios, contact }),

  postContactAvatar: ({
    contactId,
    avatarFile
  }: {
    contactId: number,
    avatarFile: File
  }) => postContactAvatar({ axios, contactId, avatarFile }),

  postContactCommentary: ({
    contactId,
    commentary
  }: {
    contactId: number,
    commentary: CommentaryForm
  }) => postContactCommentary({ axios, contactId, commentary }),

  postContactEmployee: ({
    contactId,
    employee
  }: {
    contactId: number,
    employee: ContactEmployeeForm
  }) => postContactEmployee({ axios, contactId, employee }),

  postContactEmployeeMatchQuery: ({
    contactId,
    query
  }: {
    contactId: number,
    query: ContactEmployeeMatchQuery
  }) => postContactEmployeeMatchQuery({ axios, contactId, query }),

  postContactEmployeeSuggestQuery: ({
    contactId,
    query
  }: {
    contactId: number,
    query: ContactEmployeeSuggestQuery
  }) => postContactEmployeeSuggestQuery({ axios, contactId, query }),

  postContactMatchQuery: ({
    query
  }: {
    query: ContactMatchQuery
  }) => postContactMatchQuery({ axios, query }),

  postContactShares: ({
    contactId,
    shares
  }: {
    contactId: number,
    shares: ContactSharesForm
  }) => postContactShares({ axios, contactId, shares }),

  postContactSuggestQuery: ({
    query
  }: {
    query: ContactSuggestQuery
  }) => postContactSuggestQuery({ axios, query }),

  putContact: ({
    contactId,
    contact
  }: {
    contactId: number,
    contact: ContactForm
  }) => putContact({ axios, contactId, contact }),

  putContactCommentary: ({
    contactId,
    commentaryId,
    commentary
  }: {
    contactId: number,
    commentaryId: number,
    commentary: CommentaryForm
  }) => putContactCommentary({
    axios,
    contactId,
    commentaryId,
    commentary
  }),

  putContactEmployee: ({
    contactId,
    employeeId,
    employee
  }: {
    contactId: number,
    employeeId: number,
    employee: ContactEmployeeForm
  }) => putContactEmployee({
    axios,
    contactId,
    employeeId,
    employee
  })
});

export default createContactClient;
