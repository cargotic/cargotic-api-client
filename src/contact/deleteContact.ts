import { AxiosInstance } from "axios";

import { httpDelete } from "../http";

const deleteContact = ({
  axios,
  contactId
}: {
  axios: AxiosInstance,
  contactId: number
}) => httpDelete({
  axios,
  url: `contacts/${contactId}`
});

export default deleteContact;
