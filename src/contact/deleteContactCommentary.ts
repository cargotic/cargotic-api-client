import { AxiosInstance } from "axios";

import { httpDelete } from "../http";

const deleteContactCommentary = ({
  axios,
  contactId,
  commentaryId
}: {
  axios: AxiosInstance,
  contactId: number,
  commentaryId: number
}) => httpDelete({
  axios,
  url: `contacts/${contactId}/commentaries/${commentaryId}`
});

export default deleteContactCommentary;
