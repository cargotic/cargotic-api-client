import { AxiosInstance } from "axios";

import { httpDelete } from "../http";

const deleteContactEmployee = ({
  axios,
  contactId,
  employeeId
}: {
  axios: AxiosInstance,
  contactId: number,
  employeeId: number
}) => httpDelete({
  axios,
  url: `contacts/${contactId}/employees/${employeeId}`
});

export default deleteContactEmployee;
