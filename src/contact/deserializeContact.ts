import { Contact } from "@cargotic-oss/model";

import ContactData from "./ContactData";

const deserializeContact = (
  {
    createdAt,
    insuranceExpiresAt,
    ...rest
  }: ContactData
) => ({
  ...rest,
  insuranceExpiresAt: insuranceExpiresAt
    ? new Date(insuranceExpiresAt)
    : undefined,
  createdAt: new Date(createdAt)
}) as Contact;

export default deserializeContact;
