import { ContactActivityType } from "@cargotic-oss/model";

import { deserializeCommentary } from "../social";
import ContactActivityData from "./ContactActivityData";

const deserializeContactActivity = (data: ContactActivityData) => {
  if (data.type === ContactActivityType.CONTACT_COMMENTARY) {
    return {
      ...data,
      commentary: deserializeCommentary(data.commentary)
    };
  }

  return {
    ...data,
    createdAt: new Date(data.createdAt)
  };
};

export default deserializeContactActivity;
