import { ContactMatchQueryResult } from "@cargotic-oss/model";

import ContactMatchQueryResultData from "./ContactMatchQueryResultData";
import deserializeContact from "./deserializeContact";

const deserializeContactMatchQueryResult = (
  { matches, ...rest }: ContactMatchQueryResultData
) => ({
  ...rest,
  matches: matches.map(deserializeContact)
}) as ContactMatchQueryResult;

export default deserializeContactMatchQueryResult;
