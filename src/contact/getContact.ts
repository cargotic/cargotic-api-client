import { AxiosInstance } from "axios";

import { httpGet } from "../http";
import ContactData from "./ContactData";
import deserializeContact from "./deserializeContact";

const getContact = async ({
  axios,
  contactId
}: {
  axios: AxiosInstance,
  contactId: number
}) => {
  const { data } = await httpGet<ContactData>({
    axios,
    url: `contacts/${contactId}`
  });

  return deserializeContact(data);
};

export default getContact;
