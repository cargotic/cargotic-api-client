import { ContactActivity } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpGet } from "../http";
import deserializeContactActivity from "./deserializeContactActivity";
import ContactActivityData from "./ContactActivityData";

const getContactActivity = async ({
  axios,
  contactId
}: {
  axios: AxiosInstance,
  contactId: number
}) => {
  const { data } = await httpGet<ContactActivityData[]>({
    axios,
    url: `contacts/${contactId}/activity`
  });

  return data.map(deserializeContactActivity) as ContactActivity[];
};

export default getContactActivity;
