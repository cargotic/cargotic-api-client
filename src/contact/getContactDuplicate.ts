import { ContactDuplicate } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpGet } from "../http";

const getContactDuplicate = async ({
  axios,
  ico,
  dic
}: {
  axios: AxiosInstance,
  ico?: string,
  dic?: string
}) => {
  const { data } = await httpGet<ContactDuplicate>({
    axios,
    url: "contacts/duplicate",
    params: {
      ico,
      dic
    }
  });

  return data;
};

export default getContactDuplicate;
