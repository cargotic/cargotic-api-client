import { ContactPrefill } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpGet } from "../http";

const getContactPrefill = async ({
  axios,
  ico,
  dic
}: {
  axios: AxiosInstance,
  ico?: string,
  dic?: string
}) => {
  const { data } = await httpGet<ContactPrefill>({
    axios,
    url: "contacts/prefill",
    params: {
      ico,
      dic
    }
  });

  return data;
};

export default getContactPrefill;
