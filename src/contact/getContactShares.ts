import { ContactShares } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpGet } from "../http";

const getContactShares = async ({
  axios,
  contactId
}: {
  axios: AxiosInstance,
  contactId: number
}) => {
  const { data } = await httpGet<ContactShares>({
    axios,
    url: `contacts/${contactId}/shares`
  });

  return data;
};

export default getContactShares;
