import { Currency } from "@cargotic-oss/common";
import { Highlight, TimePeriod, DateRange } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpGet } from "../http";
import { getTimeZone } from "../utility";

const getContactShipmentBalanceHighlight = async ({
  axios,
  contactId,
  currency,
  period,
  timeZone = getTimeZone(),
  range
}: {
  axios: AxiosInstance,
  contactId: number,
  currency: Currency,
  period: TimePeriod,
  timeZone?: string,
  range?: DateRange
}) => {
  const { data } = await httpGet<Highlight>({
    axios,
    url: `contacts/${contactId}/highlights/shipment-balance`,
    params: {
      currency,
      period,
      timeZone,
      range
    }
  });

  return data;
};

export default getContactShipmentBalanceHighlight;
