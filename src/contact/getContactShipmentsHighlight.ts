import { Highlight, TimePeriod, DateRange } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpGet } from "../http";
import { getTimeZone } from "../utility";

const getContactOrderShipmentsHighlight = async ({
  axios,
  contactId,
  period,
  timeZone = getTimeZone(),
  range
}: {
  axios: AxiosInstance,
  contactId: number,
  period: TimePeriod,
  timeZone?: string,
  range?: DateRange
}) => {
  const { data } = await httpGet<Highlight>({
    axios,
    url: `contacts/${contactId}/highlights/shipments`,
    params: {
      period,
      timeZone,
      range
    }
  });

  return data;
};

export default getContactOrderShipmentsHighlight;
