import { ContactTag } from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpGet } from "../http";

const getContactPrefill = async ({
  axios
}: {
  axios: AxiosInstance
}) => {
  const { data } = await httpGet<ContactTag[]>({
    axios,
    url: "contacts/tags"
  });

  return data;
};

export default getContactPrefill;
