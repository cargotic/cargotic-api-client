import { AxiosInstance } from "axios";

import { ContactForm } from "@cargotic-oss/model";

import { httpPost } from "../http";
import ContactData from "./ContactData";
import deserializeContact from "./deserializeContact";

const postContact = async ({
  axios,
  contact
}: {
  axios: AxiosInstance,
  contact: ContactForm
}) => {
  const { data } = await httpPost<ContactData>({
    axios,
    url: "contacts",
    data: contact
  });

  return deserializeContact(data);
};

export default postContact;
