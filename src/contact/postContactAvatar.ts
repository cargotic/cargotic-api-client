import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postContactAvatar = async ({
  axios,
  contactId,
  avatarFile
}: {
  axios: AxiosInstance,
  contactId: number,
  avatarFile: File
}) => {
  const form = new FormData();

  form.append("avatar", avatarFile);

  const { data } = await httpPost<{ avatarUrl: string }>({
    axios,
    url: `contacts/${contactId}/avatar`,
    data: form
  });

  return data;
};

export default postContactAvatar;
