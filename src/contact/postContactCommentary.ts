import { Commentary, CommentaryForm } from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";
import { CommentaryData, deserializeCommentary } from "../social";

const postContactCommentary = async ({
  axios,
  contactId,
  commentary
}: {
  axios: AxiosInstance,
  contactId: number,
  commentary: CommentaryForm
}) => {
  const { data } = await httpPost<CommentaryData>({
    axios,
    url: `contacts/${contactId}/commentaries`,
    data: commentary
  });

  return deserializeCommentary(data) as Partial<Commentary>;
};

export default postContactCommentary;
