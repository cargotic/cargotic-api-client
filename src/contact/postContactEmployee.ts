import { ContactEmployee, ContactEmployeeForm } from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postContactEmployee = async ({
  axios,
  contactId,
  employee
}: {
  axios: AxiosInstance,
  contactId: number,
  employee: ContactEmployeeForm
}) => {
  const { data } = await httpPost<ContactEmployee>({
    axios,
    url: `contacts/${contactId}/employees`,
    data: employee
  });

  return data;
};

export default postContactEmployee;
