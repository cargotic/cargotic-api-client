import {
  ContactEmployeeMatchQuery,
  ContactEmployeeMatchQueryResult
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postContactEmployeeMatchQuery = async ({
  axios,
  contactId,
  query
}: {
  axios: AxiosInstance,
  contactId: number,
  query: ContactEmployeeMatchQuery
}) => {
  const { data } = await httpPost<ContactEmployeeMatchQueryResult>({
    axios,
    url: `contacts/${contactId}/employees/match`,
    data: query
  });

  return data;
};

export default postContactEmployeeMatchQuery;
