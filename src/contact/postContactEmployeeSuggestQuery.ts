import {
  ContactEmployeeSuggestQuery,
  ContactEmployeeSuggestQueryResult
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postContactEmployeeSuggestQuery = async ({
  axios,
  contactId,
  query
}: {
  axios: AxiosInstance,
  contactId: number,
  query: ContactEmployeeSuggestQuery
}) => {
  const { data } = await httpPost<ContactEmployeeSuggestQueryResult>({
    axios,
    url: `contacts/${contactId}/employees/suggest`,
    data: query
  });

  return data;
};

export default postContactEmployeeSuggestQuery;
