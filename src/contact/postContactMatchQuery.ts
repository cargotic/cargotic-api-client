import { ContactMatchQuery } from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";
import ContactMatchQueryResultData from "./ContactMatchQueryResultData";
import deserializeContactMatchQueryResult
  from "./deserializeContactMatchQueryResult";

const postContactMatchQuery = async ({
  axios,
  query
}: {
  axios: AxiosInstance,
  query: ContactMatchQuery
}) => {
  const { data } = await httpPost<ContactMatchQueryResultData>({
    axios,
    url: "contacts/match",
    data: query
  });

  return deserializeContactMatchQueryResult(data);
};

export default postContactMatchQuery;
