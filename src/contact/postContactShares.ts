import { ContactSharesForm } from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postContactShares = ({
  axios,
  contactId,
  shares
}: {
  axios: AxiosInstance,
  contactId: number,
  shares: ContactSharesForm
}) => httpPost({
  axios,
  url: `contacts/${contactId}/shares`,
  data: shares
});

export default postContactShares;
