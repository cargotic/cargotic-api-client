import {
  ContactSuggestQuery,
  ContactSuggestQueryResult
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postContactSuggestQuery = async ({
  axios,
  query
}: {
  axios: AxiosInstance,
  query: ContactSuggestQuery
}) => {
  const { data } = await httpPost<ContactSuggestQueryResult>({
    axios,
    url: "contacts/suggest",
    data: query
  });

  return data;
};

export default postContactSuggestQuery;
