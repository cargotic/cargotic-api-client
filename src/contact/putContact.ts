import { ContactForm } from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPut } from "../http";
import ContactData from "./ContactData";
import deserializeContact from "./deserializeContact";

const putContact = async ({
  axios,
  contactId,
  contact
}: {
  axios: AxiosInstance,
  contactId: number,
  contact: ContactForm
}) => {
  const { data } = await httpPut<ContactData>({
    axios,
    url: `contacts/${contactId}`,
    data: contact
  });

  return deserializeContact(data);
};

export default putContact;
