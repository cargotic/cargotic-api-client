import { Commentary, CommentaryForm } from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPut } from "../http";
import { CommentaryData, deserializeCommentary } from "../social";

const putContactCommentary = async ({
  axios,
  contactId,
  commentaryId,
  commentary
}: {
  axios: AxiosInstance,
  contactId: number,
  commentaryId: number,
  commentary: CommentaryForm
}) => {
  const { data } = await httpPut<CommentaryData>({
    axios,
    url: `contacts/${contactId}/commentaries/${commentaryId}`,
    data: commentary
  });

  return deserializeCommentary(data) as Partial<Commentary>;
};

export default putContactCommentary;
