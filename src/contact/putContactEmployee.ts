import { ContactEmployee, ContactEmployeeForm } from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPut } from "../http";

const putContactEmployee = async ({
  axios,
  contactId,
  employeeId,
  employee
}: {
  axios: AxiosInstance,
  contactId: number,
  employeeId: number,
  employee: ContactEmployeeForm
}) => {
  const { data } = await httpPut<ContactEmployee>({
    axios,
    url: `contacts/${contactId}/employees/${employeeId}`,
    data: employee
  });

  return data;
};

export default putContactEmployee;
