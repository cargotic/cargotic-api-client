import Axios, { AxiosInstance } from "axios";

import { createContactClient } from "./contact";
import { createIncomingOrderClient } from "./incomingOrder";
import { createShipmentClient } from "./shipment";
import { createTrackingClient } from "./tracking";
import { createUserClient } from "./user";
import { createVehicleClient } from "./vehicle";
import { createNewsClient } from "./news";
import { createSelectedColumnsClient } from "./selectedColumns";
import { createEmailClient } from "./email";
import { createBankAccountClient } from "./bankAccount";
import { createOutcomingOrderClient } from "./outcomingOrder";
import { createFileClient } from "./file";

const DEFAULT_AXIOS_INSTANCE = Axios.create({
  headers: {
    "X-Cargotic-API-Client": "Cargotic API Client"
  }
});

const createClient = (axios: AxiosInstance = DEFAULT_AXIOS_INSTANCE) => ({
  contact: createContactClient(axios),
  incomingOrder: createIncomingOrderClient(axios),
  outcomingOrder: createOutcomingOrderClient(axios),
  shipment: createShipmentClient(axios),
  tracking: createTrackingClient(axios),
  user: createUserClient(axios),
  vehicle: createVehicleClient(axios),
  news: createNewsClient(axios),
  selectedColumns: createSelectedColumnsClient(axios),
  email: createEmailClient(axios),
  bankAccount: createBankAccountClient(axios),
  file: createFileClient(axios)
});

export default createClient;
