interface SendOrderForm {
  email: string;
  hasSendCopyToMe: boolean;
  language: string;
  message: string;
}

export default SendOrderForm;
