import { AxiosInstance } from "axios";

import postFeedback from "./postFeedback";

import postSendOrderByEmail
  from "./postSendOrderByEmail";

import SendOrderForm from "./SendOrderForm";

const createEmailClient = (axios: AxiosInstance) => ({
  postFeedback: ({
    content
  }: {
    content: string
  }) => postFeedback({
    axios,
    content
  }),

  postSendOrderByEmail: ({
    shipmentId,
    data
  }: {
    shipmentId: number,
    data: SendOrderForm
  }) => (
    postSendOrderByEmail({ axios, shipmentId, data })
  )
});

export default createEmailClient;
