import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postFeedback = async ({
  axios,
  content
}: {
  axios: AxiosInstance,
  content: string
}) => {
  await httpPost({
    axios,
    url: "email/feedback",
    data: { content }
  });
};

export default postFeedback;
