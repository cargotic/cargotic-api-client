import { AxiosInstance } from "axios";

import SendOrderForm from "./SendOrderForm";
import { httpPost } from "../http";

const postSendOrderByEmail = async ({
  axios,
  shipmentId,
  data
}: {
  axios: AxiosInstance,
  shipmentId: number,
  data: SendOrderForm
}) => {
  await httpPost({
    axios,
    url: "email/send-order",
    data: { shipmentId, ...data }
  });
};

export default postSendOrderByEmail;
