import { AxiosInstance } from "axios";

import postFile from "./postFile";

const createFileClient = (axios: AxiosInstance) => ({
  postFile: ({
    bucket,
    file
  }: {
    bucket: string,
    file: File
  }) => postFile({ axios, bucket, file })
});

export default createFileClient;
