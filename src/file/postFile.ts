import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postFile = async ({
  axios,
  bucket,
  file
}: {
  axios: AxiosInstance,
  bucket: string,
  file: File
}) => {
  const files = new FormData();

  files.append("file", file);

  const { data } = await httpPost<{ id: number, url: string }>({
    axios,
    url: `files/${bucket}`,
    data: files
  });

  return data;
};

export default postFile;
