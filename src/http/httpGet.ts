import { AxiosInstance } from "axios";

import HttpMethod from "./HttpMethod";
import httpRequest from "./httpRequest";

const httpGet = <T>({
  url,
  params,
  axios
}: {
  url: string,
  params?: object,
  axios: AxiosInstance
}) => httpRequest<T>({
  method: HttpMethod.GET,
  url,
  params,
  axios
});

export default httpGet;
