import { AxiosInstance } from "axios";

import HttpMethod from "./HttpMethod";
import httpRequest from "./httpRequest";

const httpPut = <T>({
  url,
  params,
  data,
  axios
}: {
  url: string,
  params?: object,
  data?: object,
  axios: AxiosInstance
}) => httpRequest<T>({
  url,
  params,
  data,
  axios,
  method: HttpMethod.PUT
});

export default httpPut;
