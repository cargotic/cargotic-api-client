import { AxiosInstance } from "axios";

import HttpMethod from "./HttpMethod";

const httpRequest = <T>({
  method,
  url,
  params,
  data,
  axios
}: {
  method: HttpMethod,
  url: string,
  params?: object,
  data?: object,
  axios: AxiosInstance
}) => axios.request<T>({
  method,
  url,
  params,
  data
});

export default httpRequest;
