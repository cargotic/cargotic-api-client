import { IncomingOrder } from "@cargotic-oss/model";

import { ContactData } from "../contact";

interface IncomingOrderData extends Omit<IncomingOrder,
    "updatedAt" |
    "completedAt" |
    "createdAt" |
    "customerContact"> {

    updatedAt?: string;
    completedAt?: string;
    createdAt?: string;

    customerContact: ContactData;
}

interface IncomingOrderDataWrapper {
  incomingOrder: IncomingOrderData;
}

export {
  IncomingOrderDataWrapper,
  IncomingOrderData
};
