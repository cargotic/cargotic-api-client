import { MatchQueryResult } from "@cargotic-oss/model";
import { IncomingOrderData } from "./IncomingOrderData";

type IncomingOrderMatchQueryData = MatchQueryResult<IncomingOrderData>;

export default IncomingOrderMatchQueryData;
