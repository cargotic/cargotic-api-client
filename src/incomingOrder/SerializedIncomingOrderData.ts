import { IncomingOrderForm } from "@cargotic-oss/model";

import { ContactData } from "../contact";

interface IncomingOrderData extends Omit<IncomingOrderForm, "customerContact"> {
    customerContact: ContactData;
}

export default IncomingOrderData;
