import { AxiosInstance } from "axios";

import { IncomingOrder, IncomingOrderMatchQuery } from "@cargotic-oss/model";

import deleteIncomingOrderById from "./deleteIncomingOrderById";

import getNextIncomingOrderNumber from "./getNextIncomingOrderNumber";
import getIncomingOrderById from "./getIncomingOrderById";

import postIncomingOrder from "./postIncomingOrder";
import postIncomingOrderMatchQuery from "./postIncomingOrderMatchQuery";
import postIncomingOrderFindJourneysByIds from "./postIncomingOrderFindJourneysByIds";

import putIncomingOrderById from "./putIncomingOrderById";

// import deleteIncomingOrderCommentary from "./deleteIncomingOrderCommentary";
// import getIncomingOrderActivity from "./getIncomingOrderActivity";
// import getIncomingOrderDuplicate from "./getIncomingOrderDuplicate";
// import postIncomingOrderCommentary from "./postIncomingOrderCommentary";
// import postIncomingOrderMatchQuery from "./postIncomingOrderMatchQuery";
// import postIncomingOrderPayment from "./postIncomingOrderPayment";
// import postIncomingOrderBoardMatchQuery from "./postIncomingOrderBoardMatchQuery";
// import postIncomingOrderWaypointDriveThrough
//   from "./postIncomingOrderWaypointDriveThrough";
// import deleteIncomingOrderWaypointDriveThrough
//   from "./deleteIncomingOrderWaypointDriveThrough";
// import putIncomingOrderWaypointDriveThrough
//   from "./putIncomingOrderWaypointDriveThrough";

// import putIncomingOrderCommentary from "./putIncomingOrderCommentary";

const createIncomingOrderClient = (axios: AxiosInstance) => ({
  deleteIncomingOrderById: ({ incomingOrderId }: { incomingOrderId: number }) =>
    deleteIncomingOrderById({
      axios,
      incomingOrderId
    }),

  getNextIncomingOrderNumber: () => getNextIncomingOrderNumber({ axios }),
  getIncomingOrderById: ({ incomingOrderId }: { incomingOrderId: number }) =>
    getIncomingOrderById({
      axios,
      incomingOrderId
    }),

  postIncomingOrder: ({ incomingOrder }: { incomingOrder: IncomingOrder }) =>
    postIncomingOrder({ axios, incomingOrder }),

  postIncomingOrderMatchQuery: ({ query }: { query: IncomingOrderMatchQuery }) =>
    postIncomingOrderMatchQuery({ axios, query }),

  postIncomingOrderFindJourneysByIds: ({ incomingOrderIds }: { incomingOrderIds: number[] }) =>
    postIncomingOrderFindJourneysByIds({ axios, incomingOrderIds }),

  putIncomingOrderById: ({ incomingOrderId, incomingOrder }: {
        incomingOrderId: number,
        incomingOrder: IncomingOrder
    }) =>
    putIncomingOrderById({ axios, incomingOrderId, incomingOrder })

  // deleteIncomingOrderCommentary: ({
  //   incomingOrderId,
  //   commentaryId
  // }: {
  //   incomingOrderId: number,
  //   commentaryId: number
  // }) => deleteIncomingOrderCommentary({
  //   axios,
  //   incomingOrderId,
  //   commentaryId
  // }),
  //
  //
  // getIncomingOrderActivity: ({ incomingOrderId }: { incomingOrderId: number }) => (
  //   getIncomingOrderActivity({ axios, incomingOrderId })
  // ),
  //
  // getIncomingOrderDuplicate: ({
  //   orderSerialNumber
  // }: {
  //   orderSerialNumber: string
  // }) => (
  //   getIncomingOrderDuplicate({ axios, orderSerialNumber })
  // ),
  //
  // postIncomingOrderCommentary: ({
  //   incomingOrderId,
  //   commentary
  // }: {
  //   incomingOrderId: number,
  //   commentary: CommentaryForm
  // }) => postIncomingOrderCommentary({ axios, incomingOrderId, commentary }),
  //
  // postIncomingOrderMatchQuery: ({
  //   query
  // }: {
  //   query: IncomingOrderMatchQuery
  // }) => postIncomingOrderMatchQuery({ axios, query }),
  //
  // postIncomingOrderBoardMatchQuery: ({
  //   query
  // }: {
  //   query: IncomingOrderBoardMatchQuery
  // }) => postIncomingOrderBoardMatchQuery({ axios, query }),
  //
  // postIncomingOrderPayment: ({
  //   incomingOrderId,
  //   payment
  // }: {
  //   incomingOrderId: number,
  //   payment: IncomingOrderPaymentForm
  // }) => postIncomingOrderPayment({ axios, incomingOrderId, payment }),
  //
  // postIncomingOrderWaypointDriveThrough: ({
  //   incomingOrderId,
  //   waypointId
  // }: {
  //   incomingOrderId: number,
  //   waypointId: number
  // }) => postIncomingOrderWaypointDriveThrough({
  //   axios,
  //   incomingOrderId,
  //   waypointId
  // }),
  //
  // putIncomingOrderCommentary: ({
  //   incomingOrderId,
  //   commentaryId,
  //   commentary
  // }: {
  //   incomingOrderId: number,
  //   commentaryId: number,
  //   commentary: CommentaryForm
  // }) => putIncomingOrderCommentary({
  //   axios,
  //   incomingOrderId,
  //   commentaryId,
  //   commentary
  // }),
  //
  // deleteIncomingOrderWaypointDriveThrough: ({
  //   incomingOrderId,
  //   waypointId
  // }: {
  //   incomingOrderId: number,
  //   waypointId: number
  // }) => deleteIncomingOrderWaypointDriveThrough({
  //   axios,
  //   incomingOrderId,
  //   waypointId
  // }),
  //
  // putIncomingOrderWaypointDriveThrough: ({
  //   incomingOrderId,
  //   waypointId,
  //   drivenThroughAt
  // }: {
  //   incomingOrderId: number,
  //   waypointId: number,
  //   drivenThroughAt: Date
  // }) => putIncomingOrderWaypointDriveThrough({
  //   axios,
  //   incomingOrderId,
  //   waypointId,
  //   drivenThroughAt
  // })
});

export default createIncomingOrderClient;
