import { AxiosInstance } from "axios";

import { httpDelete } from "../http";

const deleteIncomingOrderById = async (
  {
    axios,
    incomingOrderId
  }: {
        axios: AxiosInstance,
        incomingOrderId: number
    }
) => {
  const { data } = await httpDelete({
    axios,
    url: `incoming-order/${incomingOrderId}`
  });

  return data;
};

export default deleteIncomingOrderById;
