import { IncomingOrder } from "@cargotic-oss/model";

import { deserializeContact } from "../contact";
import { IncomingOrderData } from "./IncomingOrderData";

const deserializeIncomingOrder = (
  {
    createdAt,
    completedAt,
    updatedAt,
    customerContact,
    journey: {
      waypoints,
      ...restJourney
    },
    ...rest
  }: IncomingOrderData
) => ({
  ...rest,

  createdAt: createdAt ? new Date(createdAt) : undefined,
  completedAt: completedAt ? new Date(completedAt) : undefined,
  updatedAt: updatedAt ? new Date(updatedAt) : undefined,

  customerContact: customerContact
    ? deserializeContact(customerContact)
    : undefined,

  journey: {
    ...restJourney,

    waypoints: waypoints.map(
      ({
        arriveAtFrom,
        arriveAtTo,
        ...restWaypoint
      }) => ({
        ...restWaypoint,
        arriveAtFrom: arriveAtFrom ? new Date(arriveAtFrom) : undefined,
        arriveAtTo: arriveAtTo ? new Date(arriveAtTo) : undefined
      })
    )
  }
} as IncomingOrder);

export default deserializeIncomingOrder;
