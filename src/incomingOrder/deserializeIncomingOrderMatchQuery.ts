import { IncomingOrderMatchQueryResult } from "@cargotic-oss/model";

import IncomingOrderMatchQueryResultData from "./IncomingOrderMatchQueryResultData";
import deserializeIncomingOrder from "./deserializeIncomingOrder";

const deserializeIncomingOrderMatchQuery = (
  { matches, ...rest }: IncomingOrderMatchQueryResultData
) => ({
  ...rest,
  matches: matches.map(deserializeIncomingOrder)
}) as IncomingOrderMatchQueryResult;

export default deserializeIncomingOrderMatchQuery;
