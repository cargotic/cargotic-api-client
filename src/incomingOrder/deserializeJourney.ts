import { IncomingOrderData } from "./IncomingOrderData";

const deserializeJourney = (
  {
    journey: {
      waypoints,
      ...restJourney
    },
    ...rest
  }: IncomingOrderData
) => ({
  ...rest,

  journey: {
    ...restJourney,

    waypoints: waypoints.map(
      ({
        arriveAtFrom,
        arriveAtTo,
        ...restWaypoint
      }) => ({
        ...restWaypoint,
        arriveAtFrom: arriveAtFrom ? new Date(arriveAtFrom) : undefined,
        arriveAtTo: arriveAtTo ? new Date(arriveAtTo) : undefined
      })
    )
  }
});

export default deserializeJourney;
