import { AxiosInstance } from "axios";

import { httpGet } from "../http";
import { IncomingOrderDataWrapper } from "./IncomingOrderData";
import deserializeIncomingOrder from "./deserializeIncomingOrder";

const getIncomingOrderById = async ({
  axios,
  incomingOrderId
}: {
  axios: AxiosInstance,
  incomingOrderId: number,
}) => {
  const { data: { incomingOrder } } = await httpGet<IncomingOrderDataWrapper>({
    axios,
    url: `incoming-order/${incomingOrderId}`
  });

  return deserializeIncomingOrder(incomingOrder);
};

export default getIncomingOrderById;
