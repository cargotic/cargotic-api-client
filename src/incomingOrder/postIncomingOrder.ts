import { AxiosInstance } from "axios";

import { IncomingOrder } from "@cargotic-oss/model";

import { httpPost } from "../http";
import serializeIncomingOrder from "./serializeIncomingOrder";
import { IncomingOrderData } from "./IncomingOrderData";

const postIncomingOrder = async (
  {
    axios,
    incomingOrder
  }: {
        axios: AxiosInstance,
        incomingOrder: IncomingOrder
    }
) => {
  const { data } = await httpPost<IncomingOrderData>({
    axios,
    url: "incoming-order",
    data: serializeIncomingOrder(incomingOrder, false)
  });

  return data;
};

export default postIncomingOrder;
