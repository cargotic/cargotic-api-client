import { AxiosInstance } from "axios";

import { httpPost } from "../http";
import deserializeJourney from "./deserializeJourney";
import { IncomingOrderData } from "./IncomingOrderData";

const postIncomingOrderMatchQuery = async ({
  axios,
  incomingOrderIds
}: {
    axios: AxiosInstance,
    incomingOrderIds: number[]
}) => {
  const { data } = await httpPost<IncomingOrderData[]>({
    axios,
    url: "incoming-order/journeys",
    data: { incomingOrderIds }
  });

  return data.map(deserializeJourney);
};

export default postIncomingOrderMatchQuery;
