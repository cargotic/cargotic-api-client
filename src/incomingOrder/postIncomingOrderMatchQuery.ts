import {
  IncomingOrderMatchQuery
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";

import deserializeIncomingOrderMatchQuery from "./deserializeIncomingOrderMatchQuery";
import IncomingOrderMatchQueryResultData from "./IncomingOrderMatchQueryResultData";

const postIncomingOrderMatchQuery = async ({
  axios,
  query
}: {
    axios: AxiosInstance,
    query: IncomingOrderMatchQuery
}) => {
  const { data } = await httpPost<IncomingOrderMatchQueryResultData>({
    axios,
    url: "incoming-order/match",
    data: query
  });

  return deserializeIncomingOrderMatchQuery(data);
};

export default postIncomingOrderMatchQuery;
