import { AxiosInstance } from "axios";

import { IncomingOrderForm } from "@cargotic-oss/model";

import { httpPut } from "../http";
import serializeIncomingOrder from "./serializeIncomingOrder";
import deserializeIncomingOrder from "./deserializeIncomingOrder";
import { IncomingOrderData } from "./IncomingOrderData";

const putIncomingOrderById = async (
  {
    axios,
    incomingOrderId,
    incomingOrder
  }: {
        axios: AxiosInstance,
        incomingOrderId: number,
        incomingOrder: IncomingOrderForm
    }
) => {
  const { data } = await httpPut<IncomingOrderData>({
    axios,
    url: `incoming-order/${incomingOrderId}`,
    data: serializeIncomingOrder(incomingOrder, true)
  });

  return deserializeIncomingOrder(data);
};

export default putIncomingOrderById;
