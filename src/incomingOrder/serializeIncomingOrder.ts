import { IncomingOrderForm } from "@cargotic-oss/model";
import SerializedIncomingOrderData from "./SerializedIncomingOrderData";
import { ContactData } from "../contact";

const serializeIncomingOrder = (
  {
    customerContact,
    journey: {
      waypoints,
      ...restJourney
    },
    ...rest
  }: IncomingOrderForm,
  update: boolean
): SerializedIncomingOrderData => ({
  ...rest,
  // TODO: Serialize contact
  customerContact: customerContact as unknown as ContactData,

  journey: {
    ...restJourney,
    waypoints: waypoints
      .map((waypoint) => ({
        ...waypoint,
        place: {
          ...waypoint.place,
          image: undefined,
          id: update ? undefined : waypoint.place.id
        }
      }))
  }
});

export default serializeIncomingOrder;
