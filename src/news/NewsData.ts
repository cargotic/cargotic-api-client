import { News } from "@cargotic-oss/model";

interface NewsData extends Omit<News, "createdAt"> {
  createdAt: string
}

export default NewsData;
