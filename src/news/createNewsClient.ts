import { AxiosInstance } from "axios";

import getLatestNews from "./getLatestNews";
import postNewsMatchQuery from "./postNewsMatchQuery";
import postNewsSeen from "./postNewsSeen";

const createNewsClient = (axios: AxiosInstance) => ({
  getLatestNews: (
    { seen }: { seen: boolean }
  ) => getLatestNews({ axios, seen }),

  postNewsMatchQuery: () => postNewsMatchQuery({ axios }),

  postNewsSeen: () => postNewsSeen({ axios })
});

export default createNewsClient;
