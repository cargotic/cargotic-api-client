import { News } from "@cargotic-oss/model";
import NewsData from "./NewsData";

const deserializeNews = (
  { createdAt, ...rest }: NewsData
) => ({
  ...rest,
  createdAt: new Date(createdAt)
}) as News;

export default deserializeNews;
