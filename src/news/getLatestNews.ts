import { AxiosInstance } from "axios";
import { httpGet } from "../http";
import deserializeNews from "./deserializeNews";
import NewsData from "./NewsData";

const getLatestNews = async ({
  axios,
  seen = false
}: {
  axios: AxiosInstance,
  seen: boolean
}) => {
  const { data } = await httpGet<NewsData | undefined>({
    axios,
    url: "news/latest",
    params: {
      seen
    }
  });

  return data ? deserializeNews(data) : undefined;
};

export default getLatestNews;
