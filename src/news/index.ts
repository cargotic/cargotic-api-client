export { default as createNewsClient } from "./createNewsClient";
export {
  default as getLatestNews
} from "./getLatestNews";

export {
  default as postNewsMatchQuery
} from "./postNewsMatchQuery";

export { default as postNewsSeen } from "./postNewsSeen";
