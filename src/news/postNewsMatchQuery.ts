import { AxiosInstance } from "axios";
import { httpPost } from "../http";
import deserializeNews from "./deserializeNews";
import NewsData from "./NewsData";

const postNewsMatchQuery = async ({
  axios
}: {
  axios: AxiosInstance,
}) => {
  const { data } = await httpPost<NewsData[]>({
    axios,
    url: "news"
  });

  return data.map(deserializeNews);
};

export default postNewsMatchQuery;
