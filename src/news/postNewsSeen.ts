import { AxiosInstance } from "axios";
import { httpPost } from "../http";

const postNewsSeen = async ({
  axios
}: {
  axios: AxiosInstance,
}) => {
  await httpPost({
    axios,
    url: "news/seen"
  });
};

export default postNewsSeen;
