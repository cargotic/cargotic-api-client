import { OutcomingOrder } from "@cargotic-oss/model";

import { ContactData } from "../contact";

interface OutcomingOrderData extends Omit<OutcomingOrder,
    "updatedAt" |
    "completedAt" |
    "createdAt" |
    "carrierContact"> {

    updatedAt?: string;
    completedAt?: string;
    createdAt?: string;

    carrierContact: ContactData;
}

interface OutcomingOrderDataWrapper {
  outcomingOrder: OutcomingOrderData;
}

export {
  OutcomingOrderData,
  OutcomingOrderDataWrapper
};
