import { MatchQueryResult } from "@cargotic-oss/model";
import { OutcomingOrderData } from "./OutcomingOrderData";

type OutcomingOrderMatchQueryData = MatchQueryResult<OutcomingOrderData>;

export default OutcomingOrderMatchQueryData;
