import { OutcomingOrderForm } from "@cargotic-oss/model";

import { ContactData } from "../contact";

interface OutcomingOrderData extends Omit<OutcomingOrderForm, "carrierContact"> {
    carrierContact: ContactData;
}

export default OutcomingOrderData;
