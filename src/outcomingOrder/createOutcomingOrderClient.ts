import { AxiosInstance } from "axios";

import { OutcomingOrderMatchQuery, OutcomingOrder } from "@cargotic-oss/model";

import deleteOutcomingOrderById from "./deleteOutcomingOrderById";

import getNextOutcomingOrderNumber from "./getNextOutcomingOrderNumber";
import getOutcomingOrderById from "./getOutcomingOrderById";

import postOutcomingOrder from "./postOutcomingOrder";
import postOutcomingOrderMatchQuery from "./postIncomingOrderMatchQuery";

import putOutcomingOrderById from "./putOutcomingOrderById";

// import deleteOutcomingOrderCommentary from "./deleteOutcomingOrderCommentary";
// import getOutcomingOrderActivity from "./getOutcomingOrderActivity";
// import getOutcomingOrderDuplicate from "./getOutcomingOrderDuplicate";
// import postOutcomingOrderCommentary from "./postOutcomingOrderCommentary";
// import postOutcomingOrderMatchQuery from "./postOutcomingOrderMatchQuery";
// import postOutcomingOrderPayment from "./postOutcomingOrderPayment";
// import postOutcomingOrderBoardMatchQuery from "./postOutcomingOrderBoardMatchQuery";
// import postOutcomingOrderWaypointDriveThrough
//   from "./postOutcomingOrderWaypointDriveThrough";
// import deleteOutcomingOrderWaypointDriveThrough
//   from "./deleteOutcomingOrderWaypointDriveThrough";
// import putOutcomingOrderWaypointDriveThrough
//   from "./putOutcomingOrderWaypointDriveThrough";

// import putOutcomingOrderCommentary from "./putOutcomingOrderCommentary";

const createOutcomingOrderClient = (axios: AxiosInstance) => ({
  deleteOutcomingOrderById: ({ outcomingOrderId }: { outcomingOrderId: number }) =>
    deleteOutcomingOrderById({
      axios,
      outcomingOrderId
    }),

  getNextOutcomingOrderNumber: () => getNextOutcomingOrderNumber({ axios }),
  getOutcomingOrderById: ({ outcomingOrderId }: { outcomingOrderId: number }) =>
    getOutcomingOrderById({
      axios,
      outcomingOrderId
    }),

  postOutcomingOrderMatchQuery: ({ query }: { query: OutcomingOrderMatchQuery }) =>
    postOutcomingOrderMatchQuery({ axios, query }),

  postOutcomingOrder: ({ outcomingOrder }: { outcomingOrder: OutcomingOrder }) =>
    postOutcomingOrder({ axios, outcomingOrder }),

  putOutcomingOrderById: ({ outcomingOrderId, outcomingOrder }: {
        outcomingOrderId: number,
        outcomingOrder: OutcomingOrder
    }) =>
    putOutcomingOrderById({ axios, outcomingOrderId, outcomingOrder })

  // deleteOutcomingOrderCommentary: ({
  //   outcomingOrderId,
  //   commentaryId
  // }: {
  //   outcomingOrderId: number,
  //   commentaryId: number
  // }) => deleteOutcomingOrderCommentary({
  //   axios,
  //   outcomingOrderId,
  //   commentaryId
  // }),
  //
  //
  // getOutcomingOrderActivity: ({ outcomingOrderId }: { outcomingOrderId: number }) => (
  //   getOutcomingOrderActivity({ axios, outcomingOrderId })
  // ),
  //
  // getOutcomingOrderDuplicate: ({
  //   orderSerialNumber
  // }: {
  //   orderSerialNumber: string
  // }) => (
  //   getOutcomingOrderDuplicate({ axios, orderSerialNumber })
  // ),
  //
  // postOutcomingOrderCommentary: ({
  //   outcomingOrderId,
  //   commentary
  // }: {
  //   outcomingOrderId: number,
  //   commentary: CommentaryForm
  // }) => postOutcomingOrderCommentary({ axios, outcomingOrderId, commentary }),
  //
  // postOutcomingOrderMatchQuery: ({
  //   query
  // }: {
  //   query: OutcomingOrderMatchQuery
  // }) => postOutcomingOrderMatchQuery({ axios, query }),
  //
  // postOutcomingOrderBoardMatchQuery: ({
  //   query
  // }: {
  //   query: OutcomingOrderBoardMatchQuery
  // }) => postOutcomingOrderBoardMatchQuery({ axios, query }),
  //
  // postOutcomingOrderPayment: ({
  //   outcomingOrderId,
  //   payment
  // }: {
  //   outcomingOrderId: number,
  //   payment: OutcomingOrderPaymentForm
  // }) => postOutcomingOrderPayment({ axios, outcomingOrderId, payment }),
  //
  // postOutcomingOrderWaypointDriveThrough: ({
  //   outcomingOrderId,
  //   waypointId
  // }: {
  //   outcomingOrderId: number,
  //   waypointId: number
  // }) => postOutcomingOrderWaypointDriveThrough({
  //   axios,
  //   outcomingOrderId,
  //   waypointId
  // }),
  //
  // putOutcomingOrderCommentary: ({
  //   outcomingOrderId,
  //   commentaryId,
  //   commentary
  // }: {
  //   outcomingOrderId: number,
  //   commentaryId: number,
  //   commentary: CommentaryForm
  // }) => putOutcomingOrderCommentary({
  //   axios,
  //   outcomingOrderId,
  //   commentaryId,
  //   commentary
  // }),
  //
  // deleteOutcomingOrderWaypointDriveThrough: ({
  //   outcomingOrderId,
  //   waypointId
  // }: {
  //   outcomingOrderId: number,
  //   waypointId: number
  // }) => deleteOutcomingOrderWaypointDriveThrough({
  //   axios,
  //   outcomingOrderId,
  //   waypointId
  // }),
  //
  // putOutcomingOrderWaypointDriveThrough: ({
  //   outcomingOrderId,
  //   waypointId,
  //   drivenThroughAt
  // }: {
  //   outcomingOrderId: number,
  //   waypointId: number,
  //   drivenThroughAt: Date
  // }) => putOutcomingOrderWaypointDriveThrough({
  //   axios,
  //   outcomingOrderId,
  //   waypointId,
  //   drivenThroughAt
  // })
});

export default createOutcomingOrderClient;
