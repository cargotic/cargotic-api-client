import { AxiosInstance } from "axios";

import { httpDelete } from "../http";

const deleteOutcomingOrderById = async (
  {
    axios,
    outcomingOrderId
  }: {
        axios: AxiosInstance,
        outcomingOrderId: number
    }
) => {
  const { data } = await httpDelete({
    axios,
    url: `outcoming-order/${outcomingOrderId}`
  });

  return data;
};

export default deleteOutcomingOrderById;
