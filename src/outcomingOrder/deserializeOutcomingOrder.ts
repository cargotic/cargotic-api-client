import { OutcomingOrder } from "@cargotic-oss/model";

import { deserializeContact } from "../contact";
import { OutcomingOrderData } from "./OutcomingOrderData";

const deserializeOutcomingOrder = (
  {
    createdAt,
    completedAt,
    updatedAt,
    carrierContact,
    journey: {
      waypoints,
      ...restJourney
    },
    ...rest
  }: OutcomingOrderData
) => ({
  ...rest,

  createdAt: createdAt ? new Date(createdAt) : undefined,
  completedAt: completedAt ? new Date(completedAt) : undefined,
  updatedAt: updatedAt ? new Date(updatedAt) : undefined,

  carrierContact: carrierContact
    ? deserializeContact(carrierContact)
    : undefined,

  journey: {
    ...restJourney,

    waypoints: waypoints.map(
      ({
        arriveAtFrom,
        arriveAtTo,
        ...restWaypoint
      }) => ({
        ...restWaypoint,
        arriveAtFrom: arriveAtFrom ? new Date(arriveAtFrom) : undefined,
        arriveAtTo: arriveAtTo ? new Date(arriveAtTo) : undefined
      })
    )
  }
} as OutcomingOrder);

export default deserializeOutcomingOrder;
