import { OutcomingOrderMatchQueryResult } from "@cargotic-oss/model";

import OutcomingOrderMatchQueryResultData from "./OutcomingOrderMatchQueryResultData";
import deserializeOutcomingOrder from "./deserializeOutcomingOrder";

const deserializeOutcomingOrderMatchQuery = (
  { matches, ...rest }: OutcomingOrderMatchQueryResultData
) => ({
  ...rest,
  matches: matches.map(deserializeOutcomingOrder)
}) as OutcomingOrderMatchQueryResult;

export default deserializeOutcomingOrderMatchQuery;
