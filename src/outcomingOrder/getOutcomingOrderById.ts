import { AxiosInstance } from "axios";

import { httpGet } from "../http";
import { OutcomingOrderDataWrapper } from "./OutcomingOrderData";
import deserializeOutcomingOrder from "./deserializeOutcomingOrder";

const getOutcomingOrderById = async ({
  axios,
  outcomingOrderId
}: {
  axios: AxiosInstance,
  outcomingOrderId: number,
}) => {
  const { data: { outcomingOrder } } = await httpGet<OutcomingOrderDataWrapper>({
    axios,
    url: `outcoming-order/${outcomingOrderId}`
  });

  return deserializeOutcomingOrder(outcomingOrder);
};

export default getOutcomingOrderById;
