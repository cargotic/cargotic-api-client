export { default as createOutcomingOrderClient } from "./createOutcomingOrderClient";

export {
  default as deleteOutcomingOrderById
} from "./deleteOutcomingOrderById";

export {
  default as deserializeOutcomingOrder
} from "./deserializeOutcomingOrder";

export { default as getOutcomingOrderById } from "./getOutcomingOrderById";
export { default as getNextOutcomingOrderNumber } from "./getNextOutcomingOrderNumber";

export { default as postOutcomingOrder } from "./postOutcomingOrder";

export type { OutcomingOrderData, OutcomingOrderDataWrapper } from "./OutcomingOrderData";
export type { default as SerializedOutcomingOrderData } from "./SerializedOutcomingOrderData";

// export {
//   default as deleteOutcomingOrderCommentary
// } from "./deleteOutcomingOrderCommentary";

// export {
//   default as deserializeOutcomingOrderActivity
// } from "./deserializeOutcomingOrderActivity";
//
// export {
//   default as deserializeOutcomingOrderMatchQuery
// } from "./deserializeOutcomingOrderMatchQuery";

// export { default as getOutcomingOrderActivity } from "./getOutcomingOrderActivity";
// export { default as getOutcomingOrderDuplicate } from "./getOutcomingOrderDuplicate";
// export { default as postOutcomingOrderCommentary } from "./postOutcomingOrderCommentary";
// export { default as postOutcomingOrderPayment } from "./postOutcomingOrderPayment";
// export {
//   default as postOutcomingOrderBoardMatchQuery
// } from "./postOutcomingOrderBoardMatchQuery";

// export { default as putOutcomingOrderCommentary } from "./putOutcomingOrderCommentary";
// export {
//   default as postOutcomingOrderMatchQuery
// } from "./postOutcomingOrderMatchQuery";

// export {
//   default as postOutcomingOrderWaypointDriveThrough
// } from "./postOutcomingOrderWaypointDriveThrough";
// export {
//   default as deleteOutcomingOrderWaypointDriveThrough
// } from "./deleteOutcomingOrderWaypointDriveThrough";
// export {
//   default as putOutcomingOrderWaypointDriveThrough
// } from "./putOutcomingOrderWaypointDriveThrough";

// export { default as OutcomingOrderActivityData } from "./OutcomingOrderActivityData";
// export {
//   default as OutcomingOrderMatchQueryResultData
// } from "./OutcomingOrderMatchQueryResultData";
