import {
  OutcomingOrderMatchQuery
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";

import deserializeOutcomingOrderMatchQuery from "./deserializeOutcomingOrderMatchQuery";
import OutcomingOrderMatchQueryResultData from "./OutcomingOrderMatchQueryResultData";

const postOutcomingOrderMatchQuery = async ({
  axios,
  query
}: {
    axios: AxiosInstance,
    query: OutcomingOrderMatchQuery
}) => {
  const { data } = await httpPost<OutcomingOrderMatchQueryResultData>({
    axios,
    url: "outcoming-order/match",
    data: query
  });

  return deserializeOutcomingOrderMatchQuery(data);
};

export default postOutcomingOrderMatchQuery;
