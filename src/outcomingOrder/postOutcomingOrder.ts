import { AxiosInstance } from "axios";

import { OutcomingOrder } from "@cargotic-oss/model";

import { httpPost } from "../http";
import serializeOutcomingOrder from "./serializeOutcomingOrder";
import { OutcomingOrderData } from "./OutcomingOrderData";

const postOutcomingOrder = async (
  {
    axios,
    outcomingOrder
  }: {
        axios: AxiosInstance,
        outcomingOrder: OutcomingOrder
    }
) => {
  const { data } = await httpPost<OutcomingOrderData>({
    axios,
    url: "outcoming-order",
    data: serializeOutcomingOrder(outcomingOrder, false)
  });

  return data;
};

export default postOutcomingOrder;
