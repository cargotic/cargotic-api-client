import { AxiosInstance } from "axios";

import { OutcomingOrderForm } from "@cargotic-oss/model";

import { httpPut } from "../http";
import serializeOutcomingOrder from "./serializeOutcomingOrder";
import deserializeOutcomingOrder from "./deserializeOutcomingOrder";
import { OutcomingOrderData } from "./OutcomingOrderData";

const putOutcomingOrderById = async (
  {
    axios,
    outcomingOrderId,
    outcomingOrder
  }: {
        axios: AxiosInstance,
        outcomingOrderId: number,
        outcomingOrder: OutcomingOrderForm
    }
) => {
  const { data } = await httpPut<OutcomingOrderData>({
    axios,
    url: `outcoming-order/${outcomingOrderId}`,
    data: serializeOutcomingOrder(outcomingOrder, true)
  });

  return deserializeOutcomingOrder(data);
};

export default putOutcomingOrderById;
