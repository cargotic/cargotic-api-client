import { OutcomingOrderForm } from "@cargotic-oss/model";
import SerializedOutcomingOrderData from "./SerializedOutcomingOrderData";
import { ContactData } from "../contact";

const serializeOutcomingOrder = (
  {
    carrierContact,
    journey: {
      waypoints,
      ...restJourney
    },
    ...rest
  }: OutcomingOrderForm,
  update: boolean
): SerializedOutcomingOrderData => ({
  ...rest,
  // TODO: Serialize contact
  carrierContact: carrierContact as unknown as ContactData,

  journey: {
    ...restJourney,
    waypoints: waypoints
      .map((waypoint) => ({
        ...waypoint,
        place: {
          ...waypoint.place,
          image: undefined,
          id: update ? undefined : waypoint.place.id
        }
      }))
  }
});

export default serializeOutcomingOrder;
