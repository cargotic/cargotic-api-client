import { AxiosInstance } from "axios";

import { SelectedColumnsForm } from "@cargotic-oss/model";

import postSelectedColumns from "./postSelectedColumns";
import getSelectedColumns from "./getSelectedColumns";
import putSelectedColumns from "./putSelectedColumns";
import deleteSelectedColumns from "./deleteSelectedColumns";

const createSelectedColumnsClient = (axios: AxiosInstance) => ({
  createSelectedColumns: ({
    tableName,
    selectedColumns
  }: {
    tableName: string,
    selectedColumns: SelectedColumnsForm
  }) => postSelectedColumns({ axios, tableName, selectedColumns }),

  findSelectedColumns: ({
    tableName
  }: {
    tableName: string
  }) => getSelectedColumns({ axios, tableName }),

  updateSelectedColumns: ({
    tableName,
    selectedColumns
  }: {
    tableName: string,
    selectedColumns: SelectedColumnsForm
  }) => putSelectedColumns({ axios, tableName, selectedColumns }),

  deleteSelectedColumns: ({
    tableName
  }: {
    tableName: string
  }) => deleteSelectedColumns({ axios, tableName })
});

export default createSelectedColumnsClient;
