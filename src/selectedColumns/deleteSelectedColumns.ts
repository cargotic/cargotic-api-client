import { AxiosInstance } from "axios";

import { httpDelete } from "../http";

const deleteSelectedColumns = async ({
  axios,
  tableName
}: {
  axios: AxiosInstance,
  tableName: string
}) => httpDelete({
  axios,
  url: `selected-columns/${tableName}`
});

export default deleteSelectedColumns;
