import { AxiosInstance } from "axios";

import { SelectedColumns } from "@cargotic-oss/model";

import { httpGet } from "../http";

const getSelectedColumns = async ({
  axios,
  tableName
}: {
  axios: AxiosInstance,
  tableName: string
}) => {
  const { data } = await httpGet<SelectedColumns>({
    axios,
    url: `selected-columns/${tableName}`
  });

  return data;
};

export default getSelectedColumns;
