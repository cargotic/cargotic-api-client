import { AxiosInstance } from "axios";

import {
  SelectedColumns,
  SelectedColumnsForm
} from "@cargotic-oss/model";

import { httpPost } from "../http";

const postSelectedColumns = async ({
  axios,
  tableName,
  selectedColumns
}: {
  axios: AxiosInstance,
  tableName: string,
  selectedColumns: SelectedColumnsForm
}) => {
  const { data } = await httpPost<SelectedColumns>({
    axios,
    url: `selected-columns/${tableName}`,
    data: selectedColumns
  });

  return data;
};

export default postSelectedColumns;
