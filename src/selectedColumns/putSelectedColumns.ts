import { AxiosInstance } from "axios";

import {
  SelectedColumns,
  SelectedColumnsForm
} from "@cargotic-oss/model";

import { httpPut } from "../http";

const putSelectedColumns = async ({
  axios,
  tableName,
  selectedColumns
}: {
  axios: AxiosInstance,
  tableName: string,
  selectedColumns: SelectedColumnsForm
}) => {
  const { data } = await httpPut<SelectedColumns>({
    axios,
    url: `selected-columns/${tableName}`,
    data: selectedColumns
  });

  return data;
};

export default putSelectedColumns;
