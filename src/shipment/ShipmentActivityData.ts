import {
  ShipmentCommentaryActivity,
  ShipmentCreateActivity,
  ShipmentUpdateActivity
} from "@cargotic-oss/model";

import { CommentaryData } from "../social";

interface ShipmentCommentaryActivityData extends Omit<
  ShipmentCommentaryActivity,
  "commentary"
> {
  commentary: CommentaryData;
}

interface ShipmentCreateActivityData extends Omit<
  ShipmentCreateActivity,
  "createdAt"
> {
  createdAt: string;
}

interface ShipmentUpdateActivityData extends Omit<
  ShipmentUpdateActivity,
  "createdAt"
> {
  createdAt: string;
}

type ShipmentActivityData =
  ShipmentCommentaryActivityData
  | ShipmentCreateActivityData
  | ShipmentUpdateActivityData;

export default ShipmentActivityData;
