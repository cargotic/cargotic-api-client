import { ShipmentState } from "@cargotic-oss/model";

import ShipmentMatchQueryResultData from "./ShipmentMatchQueryResultData";

interface ShipmentBoardMatchQueryResultData {
  [ShipmentState.QUEUE]: ShipmentMatchQueryResultData;
  [ShipmentState.IN_PROGRESS]: ShipmentMatchQueryResultData;
  [ShipmentState.DONE]: ShipmentMatchQueryResultData;
  [ShipmentState.READY]: ShipmentMatchQueryResultData;
}

export default ShipmentBoardMatchQueryResultData;
