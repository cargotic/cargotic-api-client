import { Shipment } from "@cargotic-oss/model";

import { ContactData } from "../contact";

interface ShipmentData extends Omit<
  Shipment, "createdAt" | "carrierContact" | "customerContact"
> {
  createdAt: string;
  editedAt: string;
  carrierContact: ContactData;
  customerContact: ContactData;
}

export default ShipmentData;
