import { ShipmentMatchQueryResult } from "@cargotic-oss/model";

import ShipmentData from "./ShipmentData";

interface ShipmentMatchQueryResultData
  extends Omit<ShipmentMatchQueryResult, "matches"> {

  matches: ShipmentData[];
}

export default ShipmentMatchQueryResultData;
