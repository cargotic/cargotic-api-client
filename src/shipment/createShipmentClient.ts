import {
  CommentaryForm,
  ShipmentMatchQuery,
  ShipmentBoardMatchQuery,
  ShipmentPaymentForm
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import deleteShipmentCommentary from "./deleteShipmentCommentary";
import getNextShipmentNumber from "./getNextShipmentNumber";
import getShipmentActivity from "./getShipmentActivity";
import getShipmentDuplicate from "./getShipmentDuplicate";
import postShipmentCommentary from "./postShipmentCommentary";
import postShipmentMatchQuery from "./postShipmentMatchQuery";
import postShipmentPayment from "./postShipmentPayment";
import postShipmentBoardMatchQuery from "./postShipmentBoardMatchQuery";

import postShipmentWaypointDriveThrough
  from "./postShipmentWaypointDriveThrough";
import deleteShipmentWaypointDriveThrough
  from "./deleteShipmentWaypointDriveThrough";
import putShipmentWaypointDriveThrough
  from "./putShipmentWaypointDriveThrough";

import putShipmentCommentary from "./putShipmentCommentary";

const createShipmentClient = (axios: AxiosInstance) => ({
  deleteShipmentCommentary: ({
    shipmentId,
    commentaryId
  }: {
    shipmentId: number,
    commentaryId: number
  }) => deleteShipmentCommentary({
    axios,
    shipmentId,
    commentaryId
  }),

  getNextShipmentNumber: () => getNextShipmentNumber({ axios }),

  getShipmentActivity: ({ shipmentId }: { shipmentId: number }) => (
    getShipmentActivity({ axios, shipmentId })
  ),

  getShipmentDuplicate: ({
    orderSerialNumber
  }: {
    orderSerialNumber: string
  }) => (
    getShipmentDuplicate({ axios, orderSerialNumber })
  ),

  postShipmentCommentary: ({
    shipmentId,
    commentary
  }: {
    shipmentId: number,
    commentary: CommentaryForm
  }) => postShipmentCommentary({ axios, shipmentId, commentary }),

  postShipmentMatchQuery: ({
    query
  }: {
    query: ShipmentMatchQuery
  }) => postShipmentMatchQuery({ axios, query }),

  postShipmentBoardMatchQuery: ({
    query
  }: {
    query: ShipmentBoardMatchQuery
  }) => postShipmentBoardMatchQuery({ axios, query }),

  postShipmentPayment: ({
    shipmentId,
    payment
  }: {
    shipmentId: number,
    payment: ShipmentPaymentForm
  }) => postShipmentPayment({ axios, shipmentId, payment }),

  postShipmentWaypointDriveThrough: ({
    shipmentId,
    waypointId
  }: {
    shipmentId: number,
    waypointId: number
  }) => postShipmentWaypointDriveThrough({
    axios,
    shipmentId,
    waypointId
  }),

  putShipmentCommentary: ({
    shipmentId,
    commentaryId,
    commentary
  }: {
    shipmentId: number,
    commentaryId: number,
    commentary: CommentaryForm
  }) => putShipmentCommentary({
    axios,
    shipmentId,
    commentaryId,
    commentary
  }),

  deleteShipmentWaypointDriveThrough: ({
    shipmentId,
    waypointId
  }: {
    shipmentId: number,
    waypointId: number
  }) => deleteShipmentWaypointDriveThrough({
    axios,
    shipmentId,
    waypointId
  }),

  putShipmentWaypointDriveThrough: ({
    shipmentId,
    waypointId,
    drivenThroughAt
  }: {
    shipmentId: number,
    waypointId: number,
    drivenThroughAt: Date
  }) => putShipmentWaypointDriveThrough({
    axios,
    shipmentId,
    waypointId,
    drivenThroughAt
  })
});

export default createShipmentClient;
