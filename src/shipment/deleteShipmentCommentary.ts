import { AxiosInstance } from "axios";

import { httpDelete } from "../http";

const deleteShipmentCommentary = async ({
  axios,
  shipmentId,
  commentaryId
}: {
  axios: AxiosInstance,
  shipmentId: number,
  commentaryId: number
}) => {
  const { data } = await httpDelete({
    axios,
    url: `new-shipments/${shipmentId}/commentaries/${commentaryId}`
  });

  return data;
};

export default deleteShipmentCommentary;
