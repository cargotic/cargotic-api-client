import { AxiosInstance } from "axios";

import { httpDelete } from "../http";

const deleteShipmentWaypointDriveThrough = async ({
  axios,
  shipmentId,
  waypointId
}: {
  axios: AxiosInstance,
  shipmentId: number,
  waypointId: number
}) => {
  await httpDelete({
    axios,
    url: `new-shipments/${shipmentId}/waypoints/${waypointId}/drive-through`
  });
};

export default deleteShipmentWaypointDriveThrough;
