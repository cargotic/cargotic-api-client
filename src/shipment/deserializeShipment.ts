import { Shipment } from "@cargotic-oss/model";

import { deserializeContact } from "../contact";
import ShipmentData from "./ShipmentData";

const deserializeShipment = (
  {
    createdAt,
    editedAt,
    carrierContact,
    customerContact,
    journey: {
      waypoints,
      ...restJourney
    },
    ...rest
  }: ShipmentData
) => ({
  ...rest,
  carrierContact: carrierContact
    ? deserializeContact(carrierContact)
    : undefined,
  customerContact: customerContact
    ? deserializeContact(customerContact)
    : undefined,
  journey: {
    ...restJourney,
    waypoints: waypoints.map(({
      arriveAtFrom,
      arriveAtTo,
      ...restWaypoint
    }) => ({
      ...restWaypoint,
      arriveAtFrom: arriveAtFrom ? new Date(arriveAtFrom) : undefined,
      arriveAtTo: arriveAtTo ? new Date(arriveAtTo) : undefined
    }))
  },
  createdAt: createdAt ? new Date(createdAt) : undefined,
  editedAt: editedAt ? new Date(editedAt) : undefined
}) as Shipment;

export default deserializeShipment;
