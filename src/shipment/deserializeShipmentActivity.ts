import { ShipmentActivityType } from "@cargotic-oss/model";

import { deserializeCommentary } from "../social";
import ShipmentActivityData from "./ShipmentActivityData";

const deserializeShipmentActivity = (data: ShipmentActivityData) => {
  if (data.type === ShipmentActivityType.SHIPMENT_COMMENTARY) {
    return {
      ...data,
      commentary: deserializeCommentary(data.commentary)
    };
  }

  return {
    ...data,
    createdAt: new Date(data.createdAt)
  };
};

export default deserializeShipmentActivity;
