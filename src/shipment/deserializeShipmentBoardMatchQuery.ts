import {
  ShipmentBoardMatchQueryResult,
  ShipmentState
} from "@cargotic-oss/model";

import ShipmentBoardMatchQueryResultData
  from "./ShipmentBoardMatchQueryResultData";

import deserializeShipmentMatchQuery from "./deserializeShipmentMatchQuery";

const deserializeShipmentBoardMatchQuery = (
  shipments: ShipmentBoardMatchQueryResultData
) => ({
  ...(
    shipments.QUEUE
      ? {
        [ShipmentState.QUEUE]:
          deserializeShipmentMatchQuery(shipments.QUEUE)
      }
      : {}
  ),
  ...(
    shipments.READY
      ? {
        [ShipmentState.READY]:
          deserializeShipmentMatchQuery(shipments.READY)
      }
      : {}
  ),
  ...(
    shipments.IN_PROGRESS
      ? {
        [ShipmentState.IN_PROGRESS]:
          deserializeShipmentMatchQuery(shipments.IN_PROGRESS)
      }
      : {}
  ),
  ...(
    shipments.DONE
      ? {
        [ShipmentState.DONE]:
          deserializeShipmentMatchQuery(shipments.DONE)
      }
      : {}
  )
}) as ShipmentBoardMatchQueryResult;

export default deserializeShipmentBoardMatchQuery;
