import { ShipmentMatchQueryResult } from "@cargotic-oss/model";

import ShipmentMatchQueryResultData from "./ShipmentMatchQueryResultData";
import deserializeShipment from "./deserializeShipment";

const deserializeShipmentMatchQuery = (
  { matches, ...rest }: ShipmentMatchQueryResultData
) => ({
  ...rest,
  matches: matches.map(deserializeShipment)
}) as ShipmentMatchQueryResult;

export default deserializeShipmentMatchQuery;
