import { AxiosInstance } from "axios";

import { httpGet } from "../http";

const getNextShipmentNumber = async ({
  axios
}: {
  axios: AxiosInstance
}) => {
  const {
    data: { nextIndexNumber }
  } = await httpGet<{ nextIndexNumber: string }>({
    axios,
    url: "new-shipments/index-number"
  });

  return nextIndexNumber;
};

export default getNextShipmentNumber;
