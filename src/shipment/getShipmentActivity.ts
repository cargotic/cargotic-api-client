import { ShipmentActivity } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpGet } from "../http";
import deserializeShipmentActivity from "./deserializeShipmentActivity";
import ShipmentActivityData from "./ShipmentActivityData";

const getShipmentActivity = async ({
  axios,
  shipmentId
}: {
  axios: AxiosInstance,
  shipmentId: number
}) => {
  const { data } = await httpGet<ShipmentActivityData[]>({
    axios,
    url: `new-shipments/${shipmentId}/activity`
  });

  return data.map(deserializeShipmentActivity) as ShipmentActivity[];
};

export default getShipmentActivity;
