import { ShipmentDuplicate } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpGet } from "../http";

const getShipmentDuplicate = async ({
  axios,
  orderSerialNumber
}: {
  axios: AxiosInstance,
  orderSerialNumber: string
}) => {
  const { data } = await httpGet<ShipmentDuplicate | undefined>({
    axios,
    url: `new-shipments/duplicate/${orderSerialNumber}`
  });

  return data;
};

export default getShipmentDuplicate;
