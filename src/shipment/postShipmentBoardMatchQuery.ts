import {
  ShipmentBoardMatchQuery
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";

import deserializeShipmentBoardMatchQuery
  from "./deserializeShipmentBoardMatchQuery";

import ShipmentBoardMatchQueryResultData
  from "./ShipmentBoardMatchQueryResultData";

const postShipmentBoardMatchQuery = async ({
  axios,
  query
}: {
  axios: AxiosInstance,
  query: ShipmentBoardMatchQuery
}) => {
  const { data } = await httpPost<ShipmentBoardMatchQueryResultData>({
    axios,
    url: "new-shipments/board",
    data: query
  });

  return deserializeShipmentBoardMatchQuery(data);
};

export default postShipmentBoardMatchQuery;
