import { Commentary, CommentaryForm } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpPost } from "../http";
import { CommentaryData, deserializeCommentary } from "../social";

const postShipmentCommentary = async ({
  axios,
  shipmentId,
  commentary
}: {
  axios: AxiosInstance,
  shipmentId: number,
  commentary: CommentaryForm
}) => {
  const { data } = await httpPost<CommentaryData>({
    axios,
    url: `new-shipments/${shipmentId}/commentaries/`,
    data: commentary
  });

  return deserializeCommentary(data) as Partial<Commentary>;
};

export default postShipmentCommentary;
