import {
  ShipmentMatchQuery
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";

import deserializeShipmentMatchQuery from "./deserializeShipmentMatchQuery";
import ShipmentMatchQueryResultData from "./ShipmentMatchQueryResultData";

const postShipmentMatchQuery = async ({
  axios,
  query
}: {
  axios: AxiosInstance,
  query: ShipmentMatchQuery
}) => {
  const { data } = await httpPost<ShipmentMatchQueryResultData>({
    axios,
    url: "new-shipments/match",
    data: query
  });

  return deserializeShipmentMatchQuery(data);
};

export default postShipmentMatchQuery;
