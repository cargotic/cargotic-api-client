import { AxiosInstance } from "axios";

import { ShipmentPaymentForm } from "@cargotic-oss/model";

import { httpPost } from "../http";

const postShipmentPayment = ({
  axios,
  shipmentId,
  payment
}: {
  axios: AxiosInstance,
  shipmentId: number,
  payment: ShipmentPaymentForm
}) => httpPost<void>({
  axios,
  url: `new-shipments/${shipmentId}/payment`,
  data: payment
});

export default postShipmentPayment;
