import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postShipmentWaypointDriveThrough = async ({
  axios,
  shipmentId,
  waypointId
}: {
  axios: AxiosInstance,
  shipmentId: number,
  waypointId: number
}) => {
  const { data: { createdAt } } = await httpPost({
    axios,
    url: `new-shipments/${shipmentId}/waypoints/${waypointId}/drive-through`
  });
  return new Date(createdAt);
};

export default postShipmentWaypointDriveThrough;
