import { Commentary, CommentaryForm } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpPut } from "../http";
import { CommentaryData, deserializeCommentary } from "../social";

const putShipmentCommentary = async ({
  axios,
  shipmentId,
  commentaryId,
  commentary
}: {
  axios: AxiosInstance,
  shipmentId: number,
  commentaryId: number,
  commentary: CommentaryForm
}) => {
  const { data } = await httpPut<CommentaryData>({
    axios,
    url: `new-shipments/${shipmentId}/commentaries/${commentaryId}`,
    data: commentary
  });

  return deserializeCommentary(data) as Partial<Commentary>;
};

export default putShipmentCommentary;
