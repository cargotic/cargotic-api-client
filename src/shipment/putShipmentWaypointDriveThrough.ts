import { AxiosInstance } from "axios";

import { httpPut } from "../http";

const putShipmentWaypointDriveThrough = async ({
  axios,
  shipmentId,
  waypointId,
  drivenThroughAt
}: {
  axios: AxiosInstance,
  shipmentId: number,
  waypointId: number,
  drivenThroughAt: Date
}) => {
  const { data: { createdAt } } = await httpPut({
    axios,
    url: `new-shipments/${shipmentId}/waypoints/${waypointId}/drive-through`,
    data: { drivenThroughAt }
  });
  return new Date(createdAt);
};

export default putShipmentWaypointDriveThrough;
