import { Commentary } from "@cargotic-oss/model";

interface CommentaryData extends Omit<Commentary, "createdAt" | "editedAt"> {
  createdAt: string;
  editedAt?: string
}

export default CommentaryData;
