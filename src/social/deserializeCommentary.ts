import { Commentary } from "@cargotic-oss/model";

interface CommentaryData extends Omit<Commentary, "createdAt" | "editedAt"> {
  createdAt: string;
  editedAt?: string;
}

const deserializeCommentary = (
  { createdAt, editedAt, ...rest }: CommentaryData
) => ({
  ...rest,
  createdAt: new Date(createdAt),
  editedAt: editedAt ? new Date(editedAt) : undefined
}) as Commentary;

export default deserializeCommentary;
