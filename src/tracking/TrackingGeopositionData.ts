import { TrackingGeoposition } from "@cargotic-oss/model";

interface TrackingGeopositionData
  extends Omit<TrackingGeoposition, "trackedAt"> {

  trackedAt: string;
}

export default TrackingGeopositionData;
