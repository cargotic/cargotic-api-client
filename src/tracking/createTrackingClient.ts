import { TrackingGeoposition, TrackingForm } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import getActiveTracking from "./getActiveTracking";
import getTrackingGeopositions from "./getTrackingGeopositions";
import postTracking from "./postTracking";
import postTrackingFinish from "./postTrackingFinish";
import postTrackingGeoposition from "./postTrackingGeoposition";

const createTrackingClient = (axios: AxiosInstance) => ({
  getActiveTracking: () => getActiveTracking({
    axios
  }),

  getTrackingGeopositions: ({
    trackingId
  }: {
    trackingId: number
  }) => getTrackingGeopositions({
    axios,
    trackingId
  }),

  postTracking: ({
    tracking
  }: {
    tracking: TrackingForm
  }) => postTracking({
    axios,
    tracking
  }),

  postTrackingFinish: ({
    trackingId
  }: {
    trackingId: number
  }) => postTrackingFinish({
    axios,
    trackingId
  }),

  postTrackingGeoposition: ({
    trackingId,
    geoposition
  }: {
    trackingId: number,
    geoposition: TrackingGeoposition
  }) => postTrackingGeoposition({
    axios,
    trackingId,
    geoposition
  })
});

export default createTrackingClient;
