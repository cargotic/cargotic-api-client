import { TrackingGeoposition } from "@cargotic-oss/model";

import TrackingGeopositionData from "./TrackingGeopositionData";

const deserializeTrackingGeoposition = (
  {
    trackedAt,
    ...rest
  }: TrackingGeopositionData
) => ({
  trackedAt: new Date(trackedAt),
  ...rest
}) as TrackingGeoposition;

export default deserializeTrackingGeoposition;
