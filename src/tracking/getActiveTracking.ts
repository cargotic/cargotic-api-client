import { AxiosInstance } from "axios";

import { httpGet } from "../http";

const getActiveTracking = async ({
  axios
}: {
  axios: AxiosInstance,
}) => {
  const { data } = await httpGet({
    axios,
    url: "tracking/active"
  });

  return data;
};

export default getActiveTracking;
