import { AxiosInstance } from "axios";

import { httpGet } from "../http";
import deserializeTrackingGeoposition from "./deserializeTrackingGeoposition";
import TrackingGeopositionData from "./TrackingGeopositionData";

const getTrackingGeopositions = async ({
  axios,
  trackingId
}: {
  axios: AxiosInstance,
  trackingId: number
}) => {
  const { data } = await httpGet<TrackingGeopositionData[]>({
    axios,
    url: `tracking/${trackingId}/geopositions`
  });

  return data.map(deserializeTrackingGeoposition);
};

export default getTrackingGeopositions;
