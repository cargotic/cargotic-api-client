import { TrackingForm } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postTracking = async ({
  axios,
  tracking
}: {
  axios: AxiosInstance,
  tracking: TrackingForm
}) => {
  const { data } = await httpPost({
    axios,
    url: "tracking",
    data: tracking
  });

  return data;
};

export default postTracking;
