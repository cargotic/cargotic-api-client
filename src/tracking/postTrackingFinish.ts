import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postTrackingFinish = async ({
  axios,
  trackingId
}: {
  axios: AxiosInstance,
  trackingId: number
}) => {
  const { data } = await httpPost({
    axios,
    url: `tracking/${trackingId}/finish`
  });

  return data;
};

export default postTrackingFinish;
