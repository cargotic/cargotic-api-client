import { TrackingGeoposition } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postTrackingGeoposition = async ({
  axios,
  trackingId,
  geoposition
}: {
  axios: AxiosInstance,
  trackingId: number,
  geoposition: TrackingGeoposition
}) => {
  const { data } = await httpPost({
    axios,
    url: `tracking/${trackingId}/geoposition`,
    data: geoposition
  });

  return data;
};

export default postTrackingGeoposition;
