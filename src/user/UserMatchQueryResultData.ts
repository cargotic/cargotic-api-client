import { UserMatchQueryResult } from "@cargotic-oss/model";

import UserWithStatisticsData from "./UserWithStatisticsData";

interface UserMatchQueryResultData
  extends Omit<UserMatchQueryResult, "matches"> {

  matches: UserWithStatisticsData[];
}

export default UserMatchQueryResultData;
