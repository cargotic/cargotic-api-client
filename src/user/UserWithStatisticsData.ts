import { UserWithStatistics } from "@cargotic-oss/model";

interface UserWithStatisticsData
  extends Omit<UserWithStatistics, "lastSeenAt"> {

  lastSeenAt?: string;
}

export default UserWithStatisticsData;
