import { UserMatchQuery, UserRole, User } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import postUserMatchQuery from "./postUserMatchQuery";
import postUserSuggestQuery from "./postUserSuggestQuery";
import getUser from "./getUser";
import putUser from "./putUser";

const createUserClient = (axios: AxiosInstance) => ({
  postUserMatchQuery: ({
    query,
    timeZone
  }: {
    query: UserMatchQuery,
    timeZone?: string
  }) => postUserMatchQuery({ axios, query, timeZone }),

  postUserSuggestQuery: ({
    role,
    search
  }: {
    role?: UserRole,
    search: string
  }) => postUserSuggestQuery({ axios, role, search }),

  getUser: ({ userId }: { userId: number }) => getUser({ axios, userId }),

  putUser: ({
    userId,
    user
  }: {
    userId: number,
    user: User
  }) => putUser({ axios, userId, user })
});

export default createUserClient;
