import { UserMatchQueryResult } from "@cargotic-oss/model";

import deserializeUserWithStatistics from "./deserializeUserWithStatistics";
import UserMatchQueryResultData from "./UserMatchQueryResultData";

const deserializeUserMatchQueryResult = (
  { matches, ...rest }: UserMatchQueryResultData
) => ({
  ...rest,
  matches: matches.map(deserializeUserWithStatistics)
}) as UserMatchQueryResult;

export default deserializeUserMatchQueryResult;
