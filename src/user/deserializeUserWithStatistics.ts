import { UserWithStatistics } from "@cargotic-oss/model";

import UserWithStatisticsData from "./UserWithStatisticsData";

const deserializeUserWithStatistics = (
  { lastSeenAt, ...rest }: UserWithStatisticsData
) => ({
  ...rest,
  lastSeenAt: lastSeenAt
    ? new Date(lastSeenAt)
    : undefined
}) as UserWithStatistics;

export default deserializeUserWithStatistics;
