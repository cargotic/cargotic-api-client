import { AxiosInstance } from "axios";
import { User } from "@cargotic-oss/model";

import { httpGet } from "../http";

const getUser = async ({
  axios,
  userId
}: {
  axios: AxiosInstance,
  userId: number
}) => {
  const { data } = await httpGet<User>({
    axios,
    url: `users/${userId}`
  });

  return data;
};

export default getUser;
