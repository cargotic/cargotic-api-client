import { AxiosInstance } from "axios";
import { UserMatchQuery } from "@cargotic-oss/model";

import { httpPost } from "../http";
import { getTimeZone } from "../utility";

import deserializeUserMatchQueryResult from "./deserializeUserMatchQueryResult";
import UserMatchQueryResultData from "./UserMatchQueryResultData";

const postUserMatchQuery = async ({
  axios,
  query,
  timeZone = getTimeZone()
}: {
  axios: AxiosInstance,
  query: UserMatchQuery,
  timeZone?: string
}) => {
  const { data } = await httpPost<UserMatchQueryResultData>({
    axios,
    url: "users/match",
    params: {
      timeZone
    },
    data: query
  });

  return deserializeUserMatchQueryResult(data);
};

export default postUserMatchQuery;
