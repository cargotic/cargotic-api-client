import { AxiosInstance } from "axios";
import { UserReference, UserRole } from "@cargotic-oss/model";

import { httpPost } from "../http";

const postUserSuggestQuery = async ({
  axios,
  role,
  search
}: {
  axios: AxiosInstance,
  role?: UserRole,
  search: string
}) => {
  const { data } = await httpPost<UserReference[]>({
    axios,
    url: "users/suggest",
    data: {
      role,
      search
    }
  });

  return data;
};

export default postUserSuggestQuery;
