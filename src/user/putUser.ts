import { AxiosInstance } from "axios";
import { User } from "@cargotic-oss/model";

import { httpPut } from "../http";

const putUser = async ({
  axios,
  userId,
  user
}: {
  axios: AxiosInstance,
  userId: number,
  user: User
}) => {
  const { data } = await httpPut<User>({
    axios,
    url: `users/${userId}`,
    data: user
  });

  return data;
};

export default putUser;
