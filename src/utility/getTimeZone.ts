const getTimeZone = () => Intl.DateTimeFormat().resolvedOptions().timeZone;

export default getTimeZone;
