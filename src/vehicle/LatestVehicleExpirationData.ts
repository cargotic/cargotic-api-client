import { VehicleExpirationType } from "@cargotic-oss/model";

import VehicleExpirationData from "./VehicleExpirationData";

type LatestVehicleExpirationData = {
  [K in keyof VehicleExpirationType]?: VehicleExpirationData;
};

export default LatestVehicleExpirationData;
