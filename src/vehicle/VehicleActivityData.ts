import {
  VehicleCommentaryActivity,
  VehicleCreateActivity,
  VehicleExpenseCreateActivity,
  VehicleExpenseDeleteActivity,
  VehicleExpenseUpdateActivity,
  VehicleUpdateActivity
} from "@cargotic-oss/model";

import { CommentaryData } from "../social";

interface VehicleCommentaryActivityData extends Omit<
  VehicleCommentaryActivity,
  "commentary"
> {
  commentary: CommentaryData;
}

interface VehicleCreateActivityData extends Omit<
  VehicleCreateActivity,
  "createdAt"
> {
  createdAt: string;
}

interface VehicleExpenseCreateActivityData extends Omit<
  VehicleExpenseCreateActivity,
  "createdAt"
> {
  createdAt: string;
}

interface VehicleExpenseDeleteActivityData extends Omit<
  VehicleExpenseDeleteActivity,
  "createdAt"
> {
  createdAt: string;
}

interface VehicleExpenseUpdateActivityData extends Omit<
  VehicleExpenseUpdateActivity,
  "createdAt"
> {
  createdAt: string;
}

interface VehicleUpdateActivityData extends Omit<
  VehicleUpdateActivity,
  "createdAt"
> {
  createdAt: string;
}

type VehicleActivityData =
  VehicleCommentaryActivityData
  | VehicleCreateActivityData
  | VehicleExpenseCreateActivityData
  | VehicleExpenseDeleteActivityData
  | VehicleExpenseUpdateActivityData
  | VehicleUpdateActivityData;

export default VehicleActivityData;
