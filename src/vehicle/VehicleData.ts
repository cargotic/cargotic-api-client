import { Vehicle } from "@cargotic-oss/model";

interface VehicleData extends Omit<Vehicle, "manufacturedAt"> {
  manufacturedAt?: string;
}

export default VehicleData;
