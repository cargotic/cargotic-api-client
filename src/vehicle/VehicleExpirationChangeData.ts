import VehicleExpirationData from "./VehicleExpirationData";

interface VehicleExpirationChangeData {
  current: VehicleExpirationData;
  latest?: VehicleExpirationData;
}

export default VehicleExpirationChangeData;
