import { VehicleExpiration } from "@cargotic-oss/model";

interface VehicleExpirationData
  extends Omit<VehicleExpiration, "createdAt" | "expiresAt"> {

  createdAt?: string;
  expiresAt: string;
}

export default VehicleExpirationData;
