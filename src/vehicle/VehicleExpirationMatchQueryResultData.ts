import { VehicleExpirationMatchQueryResult } from "@cargotic-oss/model";

import VehicleExpirationData from "./VehicleExpirationData";

interface VehicleExpirationMatchQueryResultData
  extends Omit<VehicleExpirationMatchQueryResult, "matches"> {

  matches: VehicleExpirationData[];
}

export default VehicleExpirationMatchQueryResultData;
