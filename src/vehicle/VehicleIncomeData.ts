import { VehicleIncome } from "@cargotic-oss/model";

type VehicleIncomeData = Omit<VehicleIncome, "accountedAt" | "createdAt">
  & { accountedAt: string; createdAt: string; };

export default VehicleIncomeData;
