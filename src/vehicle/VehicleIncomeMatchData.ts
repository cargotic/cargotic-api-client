import { VehicleIncomeMatch } from "@cargotic-oss/model";

import VehicleIncomeData from "./VehicleIncomeData";

type VehicleIncomeMatchData = VehicleIncomeMatch
  & { income: VehicleIncomeData };

export default VehicleIncomeMatchData;
