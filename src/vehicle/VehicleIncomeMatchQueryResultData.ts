import { VehicleIncomeMatchQueryResult } from "@cargotic-oss/model";
import VehicleIncomeMatchData from "./VehicleIncomeMatchData";

interface VehicleIncomeMatchQueryResultData
  extends VehicleIncomeMatchQueryResult {

  matches: VehicleIncomeMatchData[];
}

export default VehicleIncomeMatchQueryResultData;
