import { VehicleMatchQueryResult } from "@cargotic-oss/model";

import VehicleData from "./VehicleData";

interface VehicleMatchQueryResultData
  extends Omit<VehicleMatchQueryResult, "matches"> {

  matches: VehicleData[];
}

export default VehicleMatchQueryResultData;
