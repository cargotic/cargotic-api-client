import { Currency } from "@cargotic-oss/common";

import {
  CommentaryForm,
  DirectVehicleIncomeForm,
  Id,
  MonthlyVehicleExpensesForm,
  VehicleExpirationForm,
  VehicleExpirationMatchQuery,
  VehicleForm,
  VehicleIncomeMatchQuery,
  VehicleMatchQuery,
  VehicleSuggestQuery,
  TimePeriod
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import deleteDirectVehicleIncome from "./deleteDirectVehicleIncome";
import deleteMonthlyVehicleExpenses from "./deleteMonthlyVehicleExpenses";
import deleteVehicle from "./deleteVehicle";
import deleteVehicleCommentary from "./deleteVehicleCommentary";
import deleteVehicleExpiration from "./deleteVehicleExpiration";
import getLatestVehicleExpirations from "./getLatestVehicleExpirations";
import getPreviousMonthlyVehicleExpenses from
  "./getPreviousMonthlyVehicleExpenses";
import getVehicleDuplicate from "./getVehicleDuplcate";

import getVehicle from "./getVehicle";
import getVehicleActivity from "./getVehicleActivity";
import getVehicleProfitHighlights from "./getVehicleProfitHighlights";
import getVehicleMileageHighlights from "./getVehicleMileageHighlights";
import getVehicleExpensesHighlights from "./getVehicleExpensesHighlights";
import getVehicleTravelsHighlights from "./getVehicleTravelsHighlights";
import getVehicleRevenueHighlights from "./getVehicleRevenueHighlights";
import getYearlyVehicleExpenses from "./getYearlyVehicleExpenses";
import postDirectVehicleIncome from "./postDirectVehicleIncome";
import postMonthlyVehicleExpenses from "./postMonthlyVehicleExpenses";
import postVehicle from "./postVehicle";
import postVehicleAvatar from "./postVehicleAvatar";
import postVehicleCommentary from "./postVehicleCommentary";
import postVehicleExpiration from "./postVehicleExpiration";
import postVehicleExpirationImage from "./postVehicleExpirationImage";
import postVehicleExpirationMatchQuery from "./postVehicleExpirationMatchQuery";
import postVehicleIncomeMatchQuery from "./postVehicleIncomeMatchQuery";
import postVehicleMatchQuery from "./postVehicleMatchQuery";
import postVehicleSuggestQuery from "./postVehicleSuggestQuery";
import putDirectVehicleIncome from "./putDirectVehicleIncome";
import putMonthlyVehicleExpenses from "./putMonthlyVehicleExpenses";
import putVehicle from "./putVehicle";
import putVehicleCommentary from "./putVehicleCommentary";
import putVehicleExpiration from "./putVehicleExpiration";

const createVehicleClient = (axios: AxiosInstance) => ({
  deleteDirectVehicleIncome: ({
    vehicleId,
    incomeId
  }: {
    vehicleId: Id,
    incomeId: Id
  }) => deleteDirectVehicleIncome({
    axios,
    vehicleId,
    incomeId
  }),

  deleteMonthlyVehicleExpenses: ({
    vehicleId,
    year,
    month
  }: {
    vehicleId: number,
    year: number,
    month: number
  }) => deleteMonthlyVehicleExpenses({
    axios,
    vehicleId,
    year,
    month
  }),

  deleteVehicle: ({
    vehicleId
  }: {
    vehicleId: number
  }) => deleteVehicle({ axios, vehicleId }),

  deleteVehicleCommentary: ({
    vehicleId,
    commentaryId
  }: {
    vehicleId: number,
    commentaryId: number
  }) => deleteVehicleCommentary({
    axios,
    vehicleId,
    commentaryId
  }),

  deleteVehicleExpiration: ({
    vehicleId,
    expirationId
  }: {
    vehicleId: number,
    expirationId: number
  }) => deleteVehicleExpiration({ axios, vehicleId, expirationId }),

  getLatestVehicleExpirations: ({
    vehicleId
  }: {
    vehicleId: number
  }) => getLatestVehicleExpirations({ axios, vehicleId }),

  getPreviousMonthlyVehicleExpenses: ({
    vehicleId,
    year
  }: {
    vehicleId: number,
    year: number
  }) => getPreviousMonthlyVehicleExpenses({ axios, vehicleId, year }),

  getVehicle: ({ vehicleId }: { vehicleId: number }) => (
    getVehicle({ axios, vehicleId })
  ),

  getVehicleActivity: ({ vehicleId }: { vehicleId: number }) => (
    getVehicleActivity({ axios, vehicleId })
  ),

  getVehicleExpensesHighlights: ({
    vehicleId,
    currency,
    period,
    timezone
  }: {
    vehicleId: number,
    currency: Currency,
    period: TimePeriod,
    timezone?: string
  }) => getVehicleExpensesHighlights({
    axios,
    vehicleId,
    currency,
    period,
    timezone
  }),

  getVehicleTravelsHighlights: ({
    vehicleId,
    period,
    timezone
  }: {
    vehicleId: number,
    period: TimePeriod,
    timezone?: string
  }) => getVehicleTravelsHighlights({
    axios,
    vehicleId,
    period,
    timezone
  }),

  getVehicleProfitHighlights: ({
    vehicleId,
    currency,
    period,
    timezone
  }: {
    vehicleId: number,
    currency: Currency,
    period: TimePeriod,
    timezone?: string
  }) => getVehicleProfitHighlights({
    axios,
    vehicleId,
    currency,
    period,
    timezone
  }),

  getVehicleMileageHighlights: ({
    vehicleId,
    period,
    timezone
  }: {
    vehicleId: number,
    period: TimePeriod,
    timezone?: string
  }) => getVehicleMileageHighlights({
    axios,
    vehicleId,
    period,
    timezone
  }),

  getVehicleRevenueHighlights: ({
    vehicleId,
    currency,
    period,
    timezone
  }: {
    vehicleId: number,
    currency: Currency,
    period: TimePeriod,
    timezone?: string
  }) => getVehicleRevenueHighlights({
    axios,
    vehicleId,
    currency,
    period,
    timezone
  }),

  getYearlyVehicleExpenses: ({
    vehicleId,
    year
  }: {
    vehicleId: number,
    year: number
  }) => getYearlyVehicleExpenses({ axios, vehicleId, year }),

  getVehicleDuplicate: ({
    vin
  }: {
    vin: string
  }) => getVehicleDuplicate({ axios, vin }),

  postDirectVehicleIncome: ({
    vehicleId,
    income
  }: {
    vehicleId: Id,
    income: DirectVehicleIncomeForm
  }) => postDirectVehicleIncome({
    axios,
    vehicleId,
    income
  }),

  postMonthlyVehicleExpenses: ({
    vehicleId,
    year,
    month,
    expenses
  }: {
    vehicleId: number,
    year: number,
    month: number,
    expenses: MonthlyVehicleExpensesForm
  }) => postMonthlyVehicleExpenses({
    axios,
    vehicleId,
    year,
    month,
    expenses
  }),

  postVehicle: ({
    vehicle
  }: {
    vehicle: VehicleForm
  }) => postVehicle({ axios, vehicle }),

  postVehicleAvatar: ({
    vehicleId,
    avatarFile
  }: {
    vehicleId: number,
    avatarFile: File
  }) => postVehicleAvatar({
    axios,
    vehicleId,
    avatarFile
  }),

  postVehicleCommentary: ({
    vehicleId,
    commentary
  }: {
    vehicleId: number,
    commentary: CommentaryForm
  }) => postVehicleCommentary({ axios, vehicleId, commentary }),

  postVehicleExpiration: ({
    vehicleId,
    expiration
  }: {
    vehicleId: number,
    expiration: VehicleExpirationForm
  }) => postVehicleExpiration({ axios, vehicleId, expiration }),

  postVehicleExpirationImage: ({
    imageFile
  }: {
    imageFile: File
  }) => postVehicleExpirationImage({ axios, imageFile }),

  postVehicleExpirationMatchQuery: ({
    vehicleId,
    query
  }: {
    vehicleId: number,
    query: VehicleExpirationMatchQuery
  }) => postVehicleExpirationMatchQuery({
    axios,
    vehicleId,
    query
  }),

  postVehicleIncomeMatchQuery: ({
    vehicleId,
    query
  }: {
    vehicleId: Id,
    query: VehicleIncomeMatchQuery
  }) => postVehicleIncomeMatchQuery({
    axios,
    vehicleId,
    query
  }),

  postVehicleMatchQuery: ({
    query
  }: {
    query: VehicleMatchQuery
  }) => postVehicleMatchQuery({
    axios,
    query
  }),

  postVehicleSuggestQuery: ({
    query
  }: {
    query: VehicleSuggestQuery
  }) => postVehicleSuggestQuery({ axios, query }),

  putDirectVehicleIncome: ({
    vehicleId,
    incomeId,
    income
  }: {
    vehicleId: Id,
    incomeId: Id,
    income: DirectVehicleIncomeForm
  }) => putDirectVehicleIncome({
    axios,
    vehicleId,
    incomeId,
    income
  }),

  putMonthlyVehicleExpenses: ({
    vehicleId,
    year,
    month,
    expenses
  }: {
    vehicleId: number,
    year: number,
    month: number,
    expenses: MonthlyVehicleExpensesForm
  }) => putMonthlyVehicleExpenses({
    axios,
    vehicleId,
    year,
    month,
    expenses
  }),

  putVehicle: ({
    vehicleId,
    vehicle
  }: {
    vehicleId: number,
    vehicle: VehicleForm
  }) => putVehicle({ axios, vehicleId, vehicle }),

  putVehicleCommentary: ({
    vehicleId,
    commentaryId,
    commentary
  }: {
    vehicleId: number,
    commentaryId: number,
    commentary: CommentaryForm
  }) => putVehicleCommentary({
    axios,
    vehicleId,
    commentaryId,
    commentary
  }),

  putVehicleExpiration: ({
    vehicleId,
    expirationId,
    expiration
  }: {
    vehicleId: number,
    expirationId: number,
    expiration: VehicleExpirationForm
  }) => putVehicleExpiration({
    axios,
    vehicleId,
    expirationId,
    expiration
  })
});

export default createVehicleClient;
