import { Id } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpDelete } from "../http";

const deleteDirectVehicleIncome = ({
  axios,
  vehicleId,
  incomeId
}: {
  axios: AxiosInstance,
  vehicleId: Id,
  incomeId: Id
}) => httpDelete({
  axios,
  url: `vehicles/${vehicleId}/direct-incomes/${incomeId}`
});

export default deleteDirectVehicleIncome;
