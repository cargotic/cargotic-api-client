import { AxiosInstance } from "axios";

import { httpDelete } from "../http";

const deleteMonthlyVehicleExpenses = ({
  axios,
  vehicleId,
  year,
  month
}: {
  axios: AxiosInstance,
  vehicleId: number,
  year: number,
  month: number
}) => httpDelete({
  axios,
  url: `vehicles/${vehicleId}/expenses/${year}/${month}`
});

export default deleteMonthlyVehicleExpenses;
