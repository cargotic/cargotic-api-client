import { AxiosInstance } from "axios";

import { httpDelete } from "../http";

const deleteVehicle = ({
  axios,
  vehicleId
}: {
  axios: AxiosInstance,
  vehicleId: number
}) => httpDelete({
  axios,
  url: `vehicles/${vehicleId}`
});

export default deleteVehicle;
