import { AxiosInstance } from "axios";

import { httpDelete } from "../http";

const deleteVehicleCommentary = ({
  axios,
  vehicleId,
  commentaryId
}: {
  axios: AxiosInstance,
  vehicleId: number,
  commentaryId: number
}) => httpDelete({
  axios,
  url: `vehicles/${vehicleId}/commentaries/${commentaryId}`
});

export default deleteVehicleCommentary;
