import { AxiosInstance } from "axios";

import { httpDelete } from "../http";
import deserializeVehicleExpiration from "./deserializeVehicleExpiration";
import VehicleExpirationChangeData from "./VehicleExpirationChangeData";

const deleteVehicleExpiration = async ({
  axios,
  vehicleId,
  expirationId
}: {
  axios: AxiosInstance,
  vehicleId: number,
  expirationId: number
}) => {
  const { data: { latest } } = await httpDelete<
    Pick<VehicleExpirationChangeData, "latest">
  >({
    axios,
    url: `vehicles/${vehicleId}/expirations/${expirationId}`
  });

  const deserialized = latest
    ? deserializeVehicleExpiration(latest)
    : undefined;

  return {
    latest: deserialized
  };
};

export default deleteVehicleExpiration;
