import { LatestVehicleExpirations } from "@cargotic-oss/model";

import deserializeVehicleExpiration from "./deserializeVehicleExpiration";
import LatestVehicleExpirationData from "./LatestVehicleExpirationData";

const deserializeLatestVehicleExpirations = (
  data: LatestVehicleExpirationData
) => Object.fromEntries(
  Object.entries(data)
    .map(([key, value]) => [
      key,
      value ? deserializeVehicleExpiration(value) : undefined
    ])
) as LatestVehicleExpirations;

export default deserializeLatestVehicleExpirations;
