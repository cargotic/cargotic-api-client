import { Vehicle } from "@cargotic-oss/model";

import VehicleData from "./VehicleData";

const deserializeVehicle = ({ manufacturedAt, ...rest }: VehicleData) => ({
  ...rest,
  manufacturedAt: manufacturedAt ? new Date(manufacturedAt) : undefined
}) as Vehicle;

export default deserializeVehicle;
