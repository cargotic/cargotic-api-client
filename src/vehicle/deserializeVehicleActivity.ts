import { VehicleActivityType } from "@cargotic-oss/model";

import { deserializeCommentary } from "../social";
import VehicleActivityData from "./VehicleActivityData";

const deserializeVehicleActivity = (data: VehicleActivityData) => {
  if (data.type === VehicleActivityType.VEHICLE_COMMENTARY) {
    return {
      ...data,
      commentary: deserializeCommentary(data.commentary)
    };
  }

  return {
    ...data,
    createdAt: new Date(data.createdAt)
  };
};

export default deserializeVehicleActivity;
