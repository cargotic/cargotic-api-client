import { VehicleExpiration } from "@cargotic-oss/model";

import VehicleExpirationData from "./VehicleExpirationData";

const deserializeVehicleExpiration = (
  { createdAt, expiresAt, ...rest }: VehicleExpirationData
) => ({
  ...rest,
  createdAt: createdAt ? new Date(createdAt) : undefined,
  expiresAt: new Date(expiresAt)
} as VehicleExpiration);

export default deserializeVehicleExpiration;
