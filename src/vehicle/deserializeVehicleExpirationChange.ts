import { VehicleExpirationChange } from "@cargotic-oss/model";

import deserializeVehicleExpiration from "./deserializeVehicleExpiration";
import VehicleExpirationChangeDate from "./VehicleExpirationChangeData";

const deserializeVehicleExpirationChange = (
  { current, latest }: VehicleExpirationChangeDate
) => ({
  current: current ? deserializeVehicleExpiration(current) : undefined,
  latest: latest ? deserializeVehicleExpiration(latest) : undefined
}) as VehicleExpirationChange;

export default deserializeVehicleExpirationChange;
