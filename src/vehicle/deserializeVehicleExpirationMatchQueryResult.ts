import { VehicleExpirationMatchQueryResult } from "@cargotic-oss/model";

import deserializeVehicleExpiration from "./deserializeVehicleExpiration";
import VehicleExpirationMatchQueryResultData
  from "./VehicleExpirationMatchQueryResultData";

const deserializeVehicleExpirationMatchQueryResult = (
  { matches, ...rest }: VehicleExpirationMatchQueryResultData
) => ({
  ...rest,
  matches: matches.map(deserializeVehicleExpiration)
}) as VehicleExpirationMatchQueryResult;

export default deserializeVehicleExpirationMatchQueryResult;
