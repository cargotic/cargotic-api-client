import { VehicleIncome } from "@cargotic-oss/model";

import VehicleIncomeData from "./VehicleIncomeData";

const deserializeVehicleIncome = (
  {
    accountedAt,
    createdAt,
    ...rest
  }: VehicleIncomeData
) => ({
  accountedAt: new Date(accountedAt),
  createdAt: new Date(createdAt),
  ...rest
}) as VehicleIncome;

export default deserializeVehicleIncome;
