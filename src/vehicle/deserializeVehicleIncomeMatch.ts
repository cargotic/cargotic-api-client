import { VehicleIncomeMatch } from "@cargotic-oss/model";

import deserializeVehicleIncome from "./deserializeVehicleIncome";
import VehicleIncomeMatchData from "./VehicleIncomeMatchData";

const deserializeVehicleIncomeMatch = (
  {
    income,
    ...rest
  }: VehicleIncomeMatchData
) => ({
  income: deserializeVehicleIncome(income),
  ...rest
}) as VehicleIncomeMatch;

export default deserializeVehicleIncomeMatch;
