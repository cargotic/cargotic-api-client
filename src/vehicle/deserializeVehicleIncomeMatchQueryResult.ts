import { VehicleIncomeMatchQueryResult } from "@cargotic-oss/model";

import VehicleIncomeMatchQueryResultData
  from "./VehicleIncomeMatchQueryResultData";
import deserializeVehicleIncomeMatch from "./deserializeVehicleIncomeMatch";

const deserializeVehicleIncomeMatchQueryResult = (
  {
    matches,
    ...rest
  }: VehicleIncomeMatchQueryResultData
) => ({
  matches: matches.map(deserializeVehicleIncomeMatch),
  ...rest
}) as VehicleIncomeMatchQueryResult;

export default deserializeVehicleIncomeMatchQueryResult;
