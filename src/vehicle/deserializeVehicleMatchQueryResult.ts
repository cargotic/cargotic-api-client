import { VehicleMatchQueryResult } from "@cargotic-oss/model";

import deserializeVehicle from "./deserializeVehicle";
import VehicleMatchQueryResultData
  from "./VehicleMatchQueryResultData";

const deserializeVehicleMatchQuery = (
  { matches, ...rest }: VehicleMatchQueryResultData
) => ({
  ...rest,
  matches: matches.map(deserializeVehicle)
}) as VehicleMatchQueryResult;

export default deserializeVehicleMatchQuery;
