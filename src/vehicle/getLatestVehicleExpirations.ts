import { AxiosInstance } from "axios";

import { httpGet } from "../http";
import deserializeLatestVehicleExpirations
  from "./deserializeLatestVehicleExpirations";

import LatestVehicleExpirationData from "./LatestVehicleExpirationData";

const getLatestVehicleExpirations = async ({
  axios,
  vehicleId
}: {
  axios: AxiosInstance,
  vehicleId: number
}) => {
  const { data } = await httpGet<LatestVehicleExpirationData>({
    axios,
    url: `vehicles/${vehicleId}/expirations/latest`
  });

  return deserializeLatestVehicleExpirations(data);
};

export default getLatestVehicleExpirations;
