import { MonthlyVehicleExpenses } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpGet } from "../http";

const getPreviousMonthlyVehicleExpenses = async ({
  axios,
  vehicleId,
  year
}: {
  axios: AxiosInstance,
  vehicleId: number,
  year: number
}) => {
  const { data } = await httpGet<MonthlyVehicleExpenses | null>({
    axios,
    url: `vehicles/${vehicleId}/expenses/${year}/previous`
  });

  return data ?? undefined;
};

export default getPreviousMonthlyVehicleExpenses;
