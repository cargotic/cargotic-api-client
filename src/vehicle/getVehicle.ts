import { Vehicle } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpGet } from "../http";

import deserializeVehicle from "./deserializeVehicle";
import VehicleData from "./VehicleData";

const getVehicle = async ({
  axios,
  vehicleId
}: {
  axios: AxiosInstance,
  vehicleId: number
}) => {
  const { data } = await httpGet<VehicleData>({
    axios,
    url: `vehicles/${vehicleId}`
  });

  return deserializeVehicle(data) as Partial<Vehicle>;
};

export default getVehicle;
