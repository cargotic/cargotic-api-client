import { VehicleActivity } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpGet } from "../http";
import deserializeVehicleActivity from "./deserializeVehicleActivity";
import VehicleActivityData from "./VehicleActivityData";

const getVehicleActivity = async ({
  axios,
  vehicleId
}: {
  axios: AxiosInstance,
  vehicleId: number
}) => {
  const { data } = await httpGet<VehicleActivityData[]>({
    axios,
    url: `vehicles/${vehicleId}/activity`
  });

  return data.map(deserializeVehicleActivity) as VehicleActivity[];
};

export default getVehicleActivity;
