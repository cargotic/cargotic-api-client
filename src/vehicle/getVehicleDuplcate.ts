import { VehicleReference } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpGet } from "../http";

const getVehicleDuplicate = async ({
  axios,
  vin
}: {
  axios: AxiosInstance,
  vin: string
}) => {
  const { data } = await httpGet<VehicleReference>({
    axios,
    url: "vehicles/duplicate",
    params: {
      vin
    }
  });

  return data;
};

export default getVehicleDuplicate;
