import { Highlight, TimePeriod } from "@cargotic-oss/model";
import { Currency } from "@cargotic-oss/common";

import { AxiosInstance } from "axios";

import { httpGet } from "../http";

import { getTimeZone } from "../utility";

const getVehicleExpensesHighlights = async ({
  axios,
  vehicleId,
  currency,
  period,
  timezone = getTimeZone()
}: {
  axios: AxiosInstance,
  vehicleId: number,
  currency: Currency,
  period: TimePeriod,
  timezone?: string
}) => {
  const { data } = await httpGet<Highlight>({
    axios,
    url: `vehicles/${vehicleId}/highlights/expenses`,
    params: { timezone, currency, period }
  });

  return data;
};

export default getVehicleExpensesHighlights;
