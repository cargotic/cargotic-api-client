import { Highlight, TimePeriod } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpGet } from "../http";

import { getTimeZone } from "../utility";

const getVehicleMileageHighlights = async ({
  axios,
  vehicleId,
  period,
  timezone = getTimeZone()
}: {
  axios: AxiosInstance,
  vehicleId: number,
  period: TimePeriod,
  timezone?: string
}) => {
  const { data } = await httpGet<Highlight>({
    axios,
    url: `vehicles/${vehicleId}/highlights/mileage`,
    params: { timezone, period }
  });

  return data;
};

export default getVehicleMileageHighlights;
