import { YearlyVehicleExpenses } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpGet } from "../http";

const getYearlyVehicleExpenses = async ({
  axios,
  vehicleId,
  year
}: {
  axios: AxiosInstance,
  vehicleId: number,
  year: number
}) => {
  const { data } = await httpGet<YearlyVehicleExpenses>({
    axios,
    url: `vehicles/${vehicleId}/expenses/${year}`
  });

  return data;
};

export default getYearlyVehicleExpenses;
