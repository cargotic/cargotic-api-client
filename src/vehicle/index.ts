export { default as createVehicleClient } from "./createVehicleClient";
export {
  default as deleteDirectVehicleIncome
} from "./deleteDirectVehicleIncome";

export {
  default as deleteMonthlyVehicleExpenses
} from "./deleteMonthlyVehicleExpenses";

export { default as deleteVehicle } from "./deleteVehicle";
export { default as deleteVehicleCommentary } from "./deleteVehicleCommentary";
export { default as deleteVehicleExpiration } from "./deleteVehicleExpiration";
export {
  default as deserializeLatestVehicleExpirations
} from "./deserializeLatestVehicleExpirations";

export { default as deserializeVehicle } from "./deserializeVehicle";
export {
  default as deserializeVehicleActivity
} from "./deserializeVehicleActivity";

export {
  default as deserializeVehicleExpiration
} from "./deserializeVehicleExpiration";

export {
  default as deserializeVehicleExpirationChange
} from "./deserializeVehicleExpirationChange";

export {
  default as deserializeVehicleExpirationMatchQueryResult
} from "./deserializeVehicleExpirationMatchQueryResult";

export {
  default as deserializeVehicleIncome
} from "./deserializeVehicleIncome";

export {
  default as deserializeVehicleIncomeMatch
} from "./deserializeVehicleIncomeMatch";

export {
  default as deserializeVehicleIncomeMatchQueryResult
} from "./deserializeVehicleIncomeMatchQueryResult";

export {
  default as getLatestVehicleExpirations
} from "./getLatestVehicleExpirations";

export {
  default as getPreviousMonthlyVehicleExpenses
} from "./getPreviousMonthlyVehicleExpenses";

export { default as getVehicle } from "./getVehicle";
export { default as getVehicleActivity } from "./getVehicleActivity";
export {
  default as getVehicleExpensesHighlights
} from "./getVehicleExpensesHighlights";
export {
  default as getVehicleProfitHighlights
} from "./getVehicleProfitHighlights";
export {
  default as getVehicleMileageHighlights
} from "./getVehicleMileageHighlights";
export {
  default as getVehicleTravelsHighlights
} from "./getVehicleTravelsHighlights";
export {
  default as getVehicleRevenueHighlights
} from "./getVehicleRevenueHighlights";
export {
  default as getYearlyVehicleExpenses
} from "./getYearlyVehicleExpenses";
export { default as getVehicleDuplicate } from "./getVehicleDuplcate";

export {
  default as LatestVehicleExpirationData
} from "./LatestVehicleExpirationData";

export { default as postDirectVehicleIncome } from "./postDirectVehicleIncome";
export {
  default as posetVehicleMatchQuery
} from "./postVehicleMatchQuery";

export {
  default as postMonthlyVehicleExpenses
} from "./postMonthlyVehicleExpenses";

export { default as postVehicle } from "./postVehicle";
export {
  default as postVehicleExpirationImage
} from "./postVehicleExpirationImage";

export { default as postVehicleAvatar } from "./postVehicleAvatar";
export { default as postVehicleCommentary } from "./postVehicleCommentary";
export { default as postVehicleExpiration } from "./postVehicleExpiration";
export {
  default as postVehicleExpirationMatchQuery
} from "./postVehicleExpirationMatchQuery";

export {
  default as postVehicleIncomeMatchQuery
} from "./postVehicleIncomeMatchQuery";

export { default as postVehicleSuggestQuery } from "./postVehicleSuggestQuery";
export {
  default as putMonthlyVehicleExpenses
} from "./putMonthlyVehicleExpenses";

export { default as putDirectVehicleIncome } from "./putDirectVehicleIncome";
export { default as putVehicle } from "./putVehicle";
export { default as putVehicleCommentary } from "./putVehicleCommentary";
export { default as putVehicleExpiration } from "./putVehicleExpiration";

export { default as VehicleData } from "./VehicleData";
export { default as VehicleActivityData } from "./VehicleActivityData";
export {
  default as VehicleExpirationChangeData
} from "./VehicleExpirationChangeData";

export { default as VehicleExpirationData } from "./VehicleExpirationData";
export {
  default as VehicleExpirationMatchQueryResultData
} from "./VehicleExpirationMatchQueryResultData";

export { default as VehicleIncomeData } from "./VehicleIncomeData";
export { default as VehicleIncomeMatchData } from "./VehicleIncomeMatchData";
export {
  default as VehicleIncomeMatchQueryResultData
} from "./VehicleIncomeMatchQueryResultData";
