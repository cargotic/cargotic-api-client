import {
  DirectVehicleIncomeForm,
  DirectVehicleIncome,
  Id
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";
import deserializeVehicleIncome from "./deserializeVehicleIncome";
import VehicleIncomeData from "./VehicleIncomeData";

const postDirectVehicleIncome = async ({
  axios,
  vehicleId,
  income
}: {
  axios: AxiosInstance,
  vehicleId: Id,
  income: DirectVehicleIncomeForm
}) => {
  const { data: result } = await httpPost<VehicleIncomeData>({
    axios,
    url: `vehicles/${vehicleId}/direct-incomes`,
    data: income
  });

  return deserializeVehicleIncome(result) as DirectVehicleIncome;
};

export default postDirectVehicleIncome;
