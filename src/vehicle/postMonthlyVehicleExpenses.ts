import {
  MonthlyVehicleExpenses,
  MonthlyVehicleExpensesForm
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postMonthlyVehicleExpenses = async ({
  axios,
  vehicleId,
  year,
  month,
  expenses
}: {
  axios: AxiosInstance,
  vehicleId: number,
  year: number,
  month: number,
  expenses: MonthlyVehicleExpensesForm
}) => {
  const { data } = await httpPost<MonthlyVehicleExpenses>({
    axios,
    url: `vehicles/${vehicleId}/expenses/${year}/${month}`,
    data: expenses
  });

  return data;
};

export default postMonthlyVehicleExpenses;
