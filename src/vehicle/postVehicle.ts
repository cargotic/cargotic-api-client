import { Vehicle, VehicleForm } from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postVehicle = async ({
  axios,
  vehicle
}: {
  axios: AxiosInstance,
  vehicle: VehicleForm
}) => {
  const { data } = await httpPost<Partial<Vehicle>>({
    axios,
    url: "vehicles",
    data: vehicle
  });

  return data;
};

export default postVehicle;
