import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postVehicleAvatar = async ({
  axios,
  vehicleId,
  avatarFile
}: {
  axios: AxiosInstance,
  vehicleId: number,
  avatarFile: File
}) => {
  const form = new FormData();

  form.append("avatar", avatarFile);

  const { data } = await httpPost<{ avatarUrl: string }>({
    axios,
    url: `vehicles/${vehicleId}/avatar`,
    data: form
  });

  return data;
};

export default postVehicleAvatar;
