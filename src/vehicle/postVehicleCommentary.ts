import { Commentary, CommentaryForm } from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";
import { CommentaryData, deserializeCommentary } from "../social";

const postVehicleCommentary = async ({
  axios,
  vehicleId,
  commentary
}: {
  axios: AxiosInstance,
  vehicleId: number,
  commentary: CommentaryForm
}) => {
  const { data } = await httpPost<CommentaryData>({
    axios,
    url: `vehicles/${vehicleId}/commentaries`,
    data: commentary
  });

  return deserializeCommentary(data) as Partial<Commentary>;
};

export default postVehicleCommentary;
