import { VehicleExpirationForm } from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";
import deserializeVehicleExpirationChange
  from "./deserializeVehicleExpirationChange";

import VehicleExpirationChangeData from "./VehicleExpirationChangeData";

const postVehicleExpiration = async ({
  axios,
  vehicleId,
  expiration
}: {
  axios: AxiosInstance,
  vehicleId: number,
  expiration: VehicleExpirationForm
}) => {
  const { data } = await httpPost<VehicleExpirationChangeData>({
    axios,
    url: `vehicles/${vehicleId}/expirations`,
    data: expiration
  });

  return deserializeVehicleExpirationChange(data);
};

export default postVehicleExpiration;
