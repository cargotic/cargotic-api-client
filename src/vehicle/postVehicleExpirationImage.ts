import { AxiosInstance } from "axios";

import { postFile } from "../file";

const postVehicleExpirationImage = ({
  axios,
  imageFile
}: {
  axios: AxiosInstance,
  imageFile: File
}) => postFile({
  axios,
  bucket: "vehicle-expiration-images",
  file: imageFile
});

export default postVehicleExpirationImage;
