import { VehicleExpirationMatchQuery } from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";
import deserializeVehicleExpirationMatchQueryResult
  from "./deserializeVehicleExpirationMatchQueryResult";

import VehicleExpirationMatchQueryResultData
  from "./VehicleExpirationMatchQueryResultData";

const postVehicleExpirationMatchQuery = async ({
  axios,
  vehicleId,
  query
}: {
  axios: AxiosInstance,
  vehicleId: number,
  query: VehicleExpirationMatchQuery
}) => {
  const { data } = await httpPost<VehicleExpirationMatchQueryResultData>({
    axios,
    url: `vehicles/${vehicleId}/expirations/match`,
    data: query
  });

  return deserializeVehicleExpirationMatchQueryResult(data);
};

export default postVehicleExpirationMatchQuery;
