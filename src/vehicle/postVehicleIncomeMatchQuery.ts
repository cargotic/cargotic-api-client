import { Id, VehicleIncomeMatchQuery } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpPost } from "../http";
import deserializeVehicleIncomeMatchQueryResult
  from "./deserializeVehicleIncomeMatchQueryResult";

import VehicleIncomeMatchQueryResultData
  from "./VehicleIncomeMatchQueryResultData";

const postVehicleIncomeMatchQuery = async ({
  axios,
  vehicleId,
  query
}: {
  axios: AxiosInstance,
  vehicleId: Id,
  query: VehicleIncomeMatchQuery
}) => {
  const { data: result } = await httpPost<VehicleIncomeMatchQueryResultData>({
    axios,
    url: `vehicles/${vehicleId}/incomes/match`,
    data: query
  });

  return deserializeVehicleIncomeMatchQueryResult(result);
};

export default postVehicleIncomeMatchQuery;
