import {
  VehicleMatchQuery
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";
import deserializeVehicleMatchQueryResult
  from "./deserializeVehicleMatchQueryResult";

import VehicleMatchQueryResultData
  from "./VehicleMatchQueryResultData";

const postVehicleMatchQuery = async ({
  axios,
  query
}: {
  axios: AxiosInstance,
  query: VehicleMatchQuery
}) => {
  const { data } = await httpPost<VehicleMatchQueryResultData>({
    axios,
    url: "vehicles/match",
    data: query
  });

  return deserializeVehicleMatchQueryResult(data);
};

export default postVehicleMatchQuery;
