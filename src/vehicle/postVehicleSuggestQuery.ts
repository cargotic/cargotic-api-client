import {
  VehicleSuggestQuery,
  VehicleSuggestQueryResult
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPost } from "../http";

const postVehicleSuggestQuery = async ({
  axios,
  query
}: {
  axios: AxiosInstance,
  query: VehicleSuggestQuery
}) => {
  const { data } = await httpPost<VehicleSuggestQueryResult>({
    axios,
    url: "vehicles/suggest",
    data: query
  });

  return data;
};

export default postVehicleSuggestQuery;
