import {
  DirectVehicleIncomeForm,
  DirectVehicleIncome,
  Id
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPut } from "../http";
import deserializeVehicleIncome from "./deserializeVehicleIncome";
import VehicleIncomeData from "./VehicleIncomeData";

const putDirectVehicleIncome = async ({
  axios,
  vehicleId,
  incomeId,
  income
}: {
  axios: AxiosInstance,
  vehicleId: Id,
  incomeId: Id,
  income: DirectVehicleIncomeForm
}) => {
  const { data: result } = await httpPut<VehicleIncomeData>({
    axios,
    url: `vehicles/${vehicleId}/direct-incomes/${incomeId}`,
    data: income
  });

  return deserializeVehicleIncome(result) as DirectVehicleIncome;
};

export default putDirectVehicleIncome;
