import {
  MonthlyVehicleExpenses,
  MonthlyVehicleExpensesForm
} from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPut } from "../http";

const putMonthlyVehicleExpenses = async ({
  axios,
  vehicleId,
  year,
  month,
  expenses
}: {
  axios: AxiosInstance,
  vehicleId: number,
  year: number,
  month: number,
  expenses: MonthlyVehicleExpensesForm
}) => {
  const { data } = await httpPut<MonthlyVehicleExpenses>({
    axios,
    url: `vehicles/${vehicleId}/expenses/${year}/${month}`,
    data: expenses
  });

  return data;
};

export default putMonthlyVehicleExpenses;
