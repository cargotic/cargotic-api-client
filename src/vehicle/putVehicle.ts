import { Vehicle, VehicleForm } from "@cargotic-oss/model";
import { AxiosInstance } from "axios";

import { httpPut } from "../http";

const putVehicle = async ({
  axios,
  vehicleId,
  vehicle
}: {
  axios: AxiosInstance,
  vehicleId: number,
  vehicle: VehicleForm
}) => {
  const { data } = await httpPut<Vehicle>({
    axios,
    url: `vehicles/${vehicleId}`,
    data: vehicle
  });

  return data;
};

export default putVehicle;
