import { Commentary, CommentaryForm } from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPut } from "../http";
import { CommentaryData, deserializeCommentary } from "../social";

const putVehicleCommentary = async ({
  axios,
  vehicleId,
  commentaryId,
  commentary
}: {
  axios: AxiosInstance,
  vehicleId: number,
  commentaryId: number,
  commentary: CommentaryForm
}) => {
  const { data } = await httpPut<CommentaryData>({
    axios,
    url: `vehicles/${vehicleId}/commentaries/${commentaryId}`,
    data: commentary
  });

  return deserializeCommentary(data) as Partial<Commentary>;
};

export default putVehicleCommentary;
