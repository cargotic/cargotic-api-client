import { VehicleExpirationForm } from "@cargotic-oss/model";

import { AxiosInstance } from "axios";

import { httpPut } from "../http";
import deserializeVehicleExpirationChange
  from "./deserializeVehicleExpirationChange";

import VehicleExpirationChangeData from "./VehicleExpirationChangeData";

const putVehicleExpiration = async ({
  axios,
  vehicleId,
  expirationId,
  expiration
}: {
  axios: AxiosInstance,
  vehicleId: number,
  expirationId: number,
  expiration: VehicleExpirationForm
}) => {
  const { data } = await httpPut<VehicleExpirationChangeData>({
    axios,
    url: `vehicles/${vehicleId}/expirations/${expirationId}`,
    data: expiration
  });

  return deserializeVehicleExpirationChange(data);
};

export default putVehicleExpiration;
